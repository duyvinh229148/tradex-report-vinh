package com.techx.tradex.report.repositories;

import com.techx.tradex.report.models.db.Report;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

    @Query("select r from Report r where r.status = 'PENDING'")
    List<Report> findActiveReport(Pageable pageable);
}
