package com.techx.tradex.report.repositories;

import com.techx.tradex.report.models.db.ReportHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportHistoryRepository extends JpaRepository<ReportHistory, Long> {

}
