package com.techx.tradex.report.configurations;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.minio.MinioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    private static final Logger log = LoggerFactory.getLogger(BeanConfiguration.class);

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper om = new ObjectMapper();
        om.setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        om.disable(SerializationFeature.INDENT_OUTPUT);
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return om;
    }

    @Bean
    public AmazonS3 s3Client(AppConf appConf) {
        AWSCredentials credentials =
                new BasicAWSCredentials(appConf.getS3().getAccessKey(),
                        appConf.getS3().getPrivateKey());
        Regions regions = Regions.fromName(appConf.getS3().getRegion());
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(regions)
                .build();
    }

    @Bean
    public MinioClient minioClient(AppConf appConf, ObjectMapper om) {
        try {
            MinioClient minioClient =
                    new MinioClient(
                            appConf.getMinio().getBaseUrl(),
                            appConf.getMinio().getAccessKey(),
                            appConf.getMinio().getPrivateKey());
            if (appConf.getFileServerType() != AppConf.FileServerType.minio) {
                return minioClient;
            }
            log.info("checking minio bucket exist");
            boolean isExist = minioClient.bucketExists(appConf.getMinio().getBucketName());
            if (isExist) {
                log.info("Minio bucket already exists.");
            } else {
                log.warn("Creating bucket");
                minioClient.makeBucket(appConf.getMinio().getBucketName());
                //                String policy =
                // om.writeValueAsString(appConf.getMinio().getBucketPolicy());
                String policy = appConf.getMinio().getBucketPolicy();
                log.warn("Set bucket policy '{}'", policy);
                minioClient.setBucketPolicy(appConf.getMinio().getBucketName(), policy);
            }
            if (!appConf.getMinio().getUrlRewriteTo().endsWith("/")) {
                appConf.getMinio().setUrlRewriteTo(appConf.getMinio().getUrlRewriteTo() + "/");
            }
            return minioClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
