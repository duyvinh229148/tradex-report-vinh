package com.techx.tradex.report.configurations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app")
@Data
public class AppConf {

    private String clusterId;
    private String nodeId;
    private String kafkaUrl;
    private String consumerPattern;
    private String producerPattern;
    private String uploadToFolder;
    private Long runnerDelay = 5000L;
    private FileServerType fileServerType;
    private S3 s3;
    private Minio minio;
    private Topic topic;
    public String phantomjs;
    private MarketRequestUri marketRequestUri;
    private VcscRequestUri vcscRequestUri;
    private boolean enableSendDomain = false;
    private Integer defaultKafkaTimeout;
    private String defaultSubnumber;
    private KbsvRequestUri kbsvRequestUri;
    private String futuresChart;
    private Map<String, String> reportUrls;
    private int defaultTuxedoFetchCount;
    private TradexRequestUri tradexRequestUri;
    private List<String> namespaceList;
    private String dictionaryFileDir;
    private Filter filter;
    private List<String> unchangedFilterList;
    private List<String> totalFilterList;
    private HashMap<String, String> specialTranslateHM;

    @Data
    public static class S3 {

        private String region;
        private String bucketName;
        private String accessKey;
        private String privateKey;
    }

    @Data
    public static class Minio {

        private String baseUrl;
        private String bucketName;
        private String accessKey;
        private String privateKey;
        private String urlRewriteTo;
        private String bucketPolicy;
    }

    @Data
    public static class Topic {

        private String market;
        private String tuxedo;
        private String fssRestBridge;
        private String domainConnector;
        private String notification;
        private String configuration;
        private String order;
    }

    @Data
    public static class MarketRequestUri {

        private String futuresListUri;
        private String futuresForeignerUri;
        private String indexPeriodUri;
        private String futuresPeriodUri;
    }

    @Data
    public static class VcscRequestUri {

        private String transactionHistory;
        private String accountInfo;
    }

    @Data
    public static class KbsvRequestUri {

        private String securitiesStatement;
        private String afacctnoInfor;
        private String accountMap;
        private String cashStatementHist;
        private String loanList;
        private String pnlExecuted;
        private String accountAsset;
        private String bondsToShareHist;
        private String rightOffStatement;
        private String paymentHist;
        private String advancedStatement;
        private String sellOddLotHists;
        private String stockTransferStatement;
        private String summaryAccount;
        private String securitiesPortfolio;
        private String orderHist;
        private String orderList;
        private String stopOrderHist;
        //derivaties
        private String increaseDTABalanceHist;
        private String increaseDepositAmountHist;
        private String decreaseDTABalanceHist;
        private String decreaseDepositAmountHist;
        private String overdraftDebtHist;
        private String orderMatch;
        private String orderHistFds;
        private String vimCashHist;
        private String cashOnHandHist;
        private String collateralSdtlHist;
        private String nplByDay;
        private String debtStatement;
        private String positionsStatement;
        private String positionsCalFee;
        private String orderFeeAndTax;
        private String orderListFds;
        private String accountInfo;
    }

    @Data
    public static class Filter {

        public String all;
        public String stockCode;
        public String code;
        public String symbol;
        public String sellBuyType; //("SELL" | "BUY")
        public String orderType; //("STOP" | "STOP_LIMIT")
        public String status; //("PENDING" | "COMPLETED" | "CANCELLED" | "FAILED")
        public String execType;
        public String orderStatus;
        public String via;
        public String side; //TODO
        public String uaCode; //TODO
        public String tltxCd; //TODO
    }

    @Data
    public static class TradexRequestUri {

        //configuration
        private String langResource;
    }

    public static enum FileServerType {
        s3, minio;
    }

    public String getKafkaBootstraps() {
        return this.kafkaUrl.replace(";", ",");
    }
}
