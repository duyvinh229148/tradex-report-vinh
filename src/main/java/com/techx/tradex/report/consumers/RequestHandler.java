package com.techx.tradex.report.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.kafka.KafkaRequestHandlerAndSender;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.report.Application;
import com.techx.tradex.report.configurations.AppConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequestHandler extends KafkaRequestHandlerAndSender {

    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    private ReportService reportService;

    @Autowired
    public RequestHandler(ObjectMapper objectMapper, AppConf appConf) {
        super(
                objectMapper,
                appConf.getKafkaBootstraps(),
                appConf.getClusterId(),
                appConf.getNodeId(),
                1,
                false);
    }

    @Override
    protected Object handle(Message message) {
        if (this.reportService == null) {
            this.reportService = Application.applicationContext.getBean(ReportService.class);
        }
        return reportService.receiveReportRequest(message);
    }
}
