package com.techx.tradex.report.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.exceptions.GeneralException;
import com.techx.tradex.common.exceptions.UriNotFoundException;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.common.model.notification.MethodEnum;
import com.techx.tradex.common.model.notification.NotificationMessage;
import com.techx.tradex.common.model.notification.SocketClusterConfiguration;
import com.techx.tradex.common.model.notification.SocketClusterData;
import com.techx.tradex.common.model.requests.DataRequest;
import com.techx.tradex.common.model.responses.Response;
import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.models.api.NotifyAccount;
import com.techx.tradex.report.models.api.ReportType;
import com.techx.tradex.report.models.db.Report;
import com.techx.tradex.report.models.db.ReportHistory;
import com.techx.tradex.report.models.db.ReportStatus;
import com.techx.tradex.report.models.request.GenerateRequest;
import com.techx.tradex.report.models.request.kbsv.AccountAssetRequest;
import com.techx.tradex.report.models.request.kbsv.AccountInfoRequest;
import com.techx.tradex.report.models.request.kbsv.AdvancedStatementRequest;
import com.techx.tradex.report.models.request.kbsv.BondsToShareHistRequest;
import com.techx.tradex.report.models.request.kbsv.CashOnHandHistRequest;
import com.techx.tradex.report.models.request.kbsv.CashStatementHistRequest;
import com.techx.tradex.report.models.request.kbsv.CollateralSdtlHistRequest;
import com.techx.tradex.report.models.request.kbsv.DebtStatementRequest;
import com.techx.tradex.report.models.request.kbsv.DecreaseDTABalanceHistRequest;
import com.techx.tradex.report.models.request.kbsv.DecreaseDepositAmountHistRequest;
import com.techx.tradex.report.models.request.kbsv.IncreaseDTABalanceHistRequest;
import com.techx.tradex.report.models.request.kbsv.IncreaseDepositAmountHistRequest;
import com.techx.tradex.report.models.request.kbsv.LoanListRequest;
import com.techx.tradex.report.models.request.kbsv.NplByDayRequest;
import com.techx.tradex.report.models.request.kbsv.OrderFeeAndTaxRequest;
import com.techx.tradex.report.models.request.kbsv.OrderHistFdsRequest;
import com.techx.tradex.report.models.request.kbsv.OrderHistRequest;
import com.techx.tradex.report.models.request.kbsv.OrderListFdsRequest;
import com.techx.tradex.report.models.request.kbsv.OrderListRequest;
import com.techx.tradex.report.models.request.kbsv.OrderMatchRequest;
import com.techx.tradex.report.models.request.kbsv.OverdraftDebtHistRequest;
import com.techx.tradex.report.models.request.kbsv.PaymentHistRequest;
import com.techx.tradex.report.models.request.kbsv.PnlExecutedRequest;
import com.techx.tradex.report.models.request.kbsv.PositionsCalFeeRequest;
import com.techx.tradex.report.models.request.kbsv.PositionsStatementRequest;
import com.techx.tradex.report.models.request.kbsv.RightOffStatementRequest;
import com.techx.tradex.report.models.request.kbsv.SecuritiesPortfolioRequest;
import com.techx.tradex.report.models.request.kbsv.SecuritiesStatementRequest;
import com.techx.tradex.report.models.request.kbsv.SellOddLotHistsRequest;
import com.techx.tradex.report.models.request.kbsv.StockTransferStatementRequest;
import com.techx.tradex.report.models.request.kbsv.StopOrderHistRequest;
import com.techx.tradex.report.models.request.kbsv.SummaryAccountRequest;
import com.techx.tradex.report.models.request.kbsv.VimCashHistRequest;
import com.techx.tradex.report.models.request.vcsc.TransactionStatementReportRequest;
import com.techx.tradex.report.models.response.GenerateResponse;
import com.techx.tradex.report.models.response.ReportResultResponse;
import com.techx.tradex.report.repositories.ReportHistoryRepository;
import com.techx.tradex.report.repositories.ReportRepository;
import com.techx.tradex.report.services.FuturesReportService;
import com.techx.tradex.report.services.GenerateReportService;
import com.techx.tradex.report.services.KbsvReportService;
import com.techx.tradex.report.services.VcscReportService;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.OptimisticLockException;
import javax.transaction.Transactional;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ReportRunnerService {

    private static final Logger log = LoggerFactory.getLogger(ReportRunnerService.class);
    private AppConf appConf;
    private ReportRepository reportRepository;
    private ReportHistoryRepository reportHistoryRepository;
    private EntityManager entityManager;
    private ObjectMapper objectMapper;
    private RequestHandler requestHandler;
    private GenerateReportService generateReportService;
    private FuturesReportService futuresReportService;
    private KbsvReportService kbsvReportService;
    private VcscReportService vcscReportService;

    @Autowired
    public ReportRunnerService(
            ObjectMapper objectMapper,
            AppConf appConf,
            EntityManager entityManager,
            RequestHandler requestHandler,
            GenerateReportService generateReportService,
            FuturesReportService futuresReportService,
            ReportHistoryRepository reportHistoryRepository,
            ReportRepository reportRepository,
            KbsvReportService kbsvReportService,
            VcscReportService vcscReportService) {

        this.objectMapper = objectMapper;
        this.appConf = appConf;
        this.requestHandler = requestHandler;
        this.entityManager = entityManager;
        this.reportRepository = reportRepository;
        this.generateReportService = generateReportService;
        this.futuresReportService = futuresReportService;
        this.reportHistoryRepository = reportHistoryRepository;
        this.kbsvReportService = kbsvReportService;
        this.vcscReportService = vcscReportService;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean run() {
        List<Report> reports =
                this.reportRepository.findActiveReport(
                        PageRequest.of(0, 1, Sort.by(Sort.Direction.ASC, "id")));
        if (reports.isEmpty()) {
            return false;
        }
        Report report = reports.get(0);

        LockModeType lockMode = this.entityManager.getLockMode(report);

        if (lockMode == LockModeType.PESSIMISTIC_WRITE) {
            return true;
        }
        try {
            this.entityManager.lock(report, LockModeType.PESSIMISTIC_WRITE);
        } catch (OptimisticLockException e) {
            if (e.getCause() != null && e.getCause() instanceof StaleObjectStateException) {
                log.warn("seem report {} is locked by another process. message {}", report.getId(),
                        e.getMessage());
            } else {
                log.warn("trying to lock report {} but failed with message {}", report.getId(),
                        e.getMessage());
            }
        }
        report.setStatus(ReportStatus.RUNNING);
        report.setStartedAt(new Timestamp(System.currentTimeMillis()));

        this.reportRepository.save(report);
        this.entityManager.flush();
        log.info("start processing report {}", report.getId());
        ReportHistory reportHistory = new ReportHistory();
        reportHistory.setId(report.getId());
        reportHistory.setJsonRequest(report.getJsonRequest());
        reportHistory.setStartedAt(report.getStartedAt());
        Object request = null;
        ReportResultResponse result = new ReportResultResponse();
        result.setId(report.getId());
        try {
            Message message = this.objectMapper.readValue(report.getJsonRequest(), Message.class);
            CompletableFuture<GenerateResponse> fut = null;
            if (message.getUri().equalsIgnoreCase("/api/v1/report/generate")) {
                request = getData(message, GenerateRequest.class);
                fut = this.generateReportService.generateReport((GenerateRequest) request);
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/generate/futures")) {
                fut = this.futuresReportService.generateReport();
            }
            // vcsc
            else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/vcsc/transactionStatement")) {
                request = getData(message, TransactionStatementReportRequest.class);
                fut =
                        this.vcscReportService.transactionStatement(
                                (TransactionStatementReportRequest) request);
            }
            // Kbsv
            else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/securitiesStatement")) {
                request = getData(message, SecuritiesStatementRequest.class);
                fut = this.kbsvReportService
                        .securitiesStatement((SecuritiesStatementRequest) request,
                                getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/cashStatementHist")) {
                request = getData(message, CashStatementHistRequest.class);
                fut = this.kbsvReportService.cashStatementHist((CashStatementHistRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/loanList")) {
                request = getData(message, LoanListRequest.class);
                fut = this.kbsvReportService
                        .loanList((LoanListRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/paymentHist")) {
                request = getData(message, PaymentHistRequest.class);
                fut = this.kbsvReportService
                        .paymentHist((PaymentHistRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/pnlExecuted")) {
                request = getData(message, PnlExecutedRequest.class);
                fut = this.kbsvReportService
                        .pnlExecuted((PnlExecutedRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/advancedStatement")) {
                request = getData(message, AdvancedStatementRequest.class);
                fut = this.kbsvReportService.advancedStatement((AdvancedStatementRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/rightOffStatement")) {
                request = getData(message, RightOffStatementRequest.class);
                fut = this.kbsvReportService.rightOffStatement((RightOffStatementRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/accountAsset")) {
                request = getData(message, AccountAssetRequest.class);
                fut = this.kbsvReportService
                        .accountAsset((AccountAssetRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/bondsToShareHist")) {
                request = getData(message, BondsToShareHistRequest.class);
                fut = this.kbsvReportService.bondsToShareHist((BondsToShareHistRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/stockTransferStatement")) {
                request = getData(message, StockTransferStatementRequest.class);
                fut =
                        this.kbsvReportService
                                .stockTransferStatement((StockTransferStatementRequest) request,
                                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/sellOddLotHists")) {
                request = getData(message, SellOddLotHistsRequest.class);
                fut = this.kbsvReportService.sellOddLotHists((SellOddLotHistsRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/securitiesPortfolio")) {
                request = getData(message, SecuritiesPortfolioRequest.class);
                fut = this.kbsvReportService
                        .securitiesPortfolio((SecuritiesPortfolioRequest) request,
                                getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/summaryAccount")) {
                request = getData(message, SummaryAccountRequest.class);
                fut = this.kbsvReportService.summaryAccount((SummaryAccountRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/orderHist")) {
                request = getData(message, OrderHistRequest.class);
                fut = this.kbsvReportService
                        .orderHist((OrderHistRequest) request, getAcceptLanguage(message));

            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/orderList")) {
                request = getData(message, OrderListRequest.class);
                fut = this.kbsvReportService
                        .orderList((OrderListRequest) request, getAcceptLanguage(message));

            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/stopOrderHist")) {
                request = getData(message, StopOrderHistRequest.class);
                fut = this.kbsvReportService
                        .stopOrderHist((StopOrderHistRequest) request, getAcceptLanguage(message));

                // derivative
            } else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/increaseDTABalanceHist")) {
                request = getData(message, IncreaseDTABalanceHistRequest.class);
                fut =
                        this.kbsvReportService
                                .increaseDTABalanceHist((IncreaseDTABalanceHistRequest) request,
                                        getAcceptLanguage(message));
            } else if (message
                    .getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/increaseDepositAmountHist")) {
                request = getData(message, IncreaseDepositAmountHistRequest.class);
                fut =
                        this.kbsvReportService.increaseDepositAmountHist(
                                (IncreaseDepositAmountHistRequest) request,
                                getAcceptLanguage(message));
            } else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/decreaseDTABalanceHist")) {
                request = getData(message, DecreaseDTABalanceHistRequest.class);
                fut =
                        this.kbsvReportService
                                .decreaseDTABalanceHist((DecreaseDTABalanceHistRequest) request,
                                        getAcceptLanguage(message));
            } else if (message
                    .getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/decreaseDepositAmountHist")) {
                request = getData(message, DecreaseDepositAmountHistRequest.class);
                fut =
                        this.kbsvReportService.decreaseDepositAmountHist(
                                (DecreaseDepositAmountHistRequest) request,
                                getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/overdraftDebtHist")) {
                request = getData(message, OverdraftDebtHistRequest.class);
                fut = this.kbsvReportService.overdraftDebtHist((OverdraftDebtHistRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/orderMatch")) {
                request = getData(message, OrderMatchRequest.class);
                fut = this.kbsvReportService
                        .orderMatch((OrderMatchRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/orderHistFds")) {
                request = getData(message, OrderHistFdsRequest.class);
                fut = this.kbsvReportService
                        .orderHistFds((OrderHistFdsRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/vimCashHist")) {
                request = getData(message, VimCashHistRequest.class);
                fut = this.kbsvReportService
                        .vimCashHist((VimCashHistRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/cashOnHandHist")) {
                request = getData(message, CashOnHandHistRequest.class);
                fut = this.kbsvReportService.cashOnHandHist((CashOnHandHistRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/collateralSdtlHist")) {
                request = getData(message, CollateralSdtlHistRequest.class);
                fut = this.kbsvReportService
                        .collateralSdtlHist((CollateralSdtlHistRequest) request,
                                getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/nplByDay")) {
                request = getData(message, NplByDayRequest.class);
                fut = this.kbsvReportService
                        .nplByDay((NplByDayRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/debtStatement")) {
                request = getData(message, DebtStatementRequest.class);
                fut = this.kbsvReportService
                        .debtStatement((DebtStatementRequest) request, getAcceptLanguage(message));
            } else if (message.getUri()
                    .equalsIgnoreCase("/api/v1/report/kbsv/positionsStatement")) {
                request = getData(message, PositionsStatementRequest.class);
                fut = this.kbsvReportService
                        .positionsStatement((PositionsStatementRequest) request,
                                getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/positionsCalFee")) {
                request = getData(message, PositionsCalFeeRequest.class);
                fut = this.kbsvReportService.positionsCalFee((PositionsCalFeeRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/orderFeeAndTax")) {
                request = getData(message, OrderFeeAndTaxRequest.class);
                fut = this.kbsvReportService.orderFeeAndTax((OrderFeeAndTaxRequest) request,
                        getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/orderListFds")) {
                request = getData(message, OrderListFdsRequest.class);
                fut = this.kbsvReportService
                        .orderListFds((OrderListFdsRequest) request, getAcceptLanguage(message));
            } else if (message.getUri().equalsIgnoreCase("/api/v1/report/kbsv/accountInfo")) {
                request = getData(message, AccountInfoRequest.class);
                fut = this.kbsvReportService
                        .accountInfo((AccountInfoRequest) request, getAcceptLanguage(message));
            }
            if (fut == null) {
                throw new UriNotFoundException();
            }
            if (request instanceof ReportType) {
                result.setType(((ReportType) request).getReportType());
            } else {
                result.setType(request.getClass().getSimpleName());
            }
            GenerateResponse response = fut.join();
            result.setResponse(new Response<>(response));

            reportHistory.setResult(this.objectMapper.writeValueAsString(result));
            reportHistory.setStatus(ReportStatus.SUCCESS);
            log.info("finish processing report {} SUCCESS", report.getId());
        } catch (Exception e) {
            result.setResponse((Response<GenerateResponse>) Response.fromException(e));
            try {
                reportHistory.setResult(this.objectMapper.writeValueAsString(result));
            } catch (Exception err) {
                log.error("fail to write exception {} to json", e.getClass(), err);
                reportHistory.setResult(e.getMessage());
            }
            log.error("fail to generate report {}", report.getId(), e);
            reportHistory.setStatus(ReportStatus.FAILED);
        }
        reportHistory.setCreatedAt(report.getCreatedAt());
        reportHistory.setFinishedAt(new Timestamp(System.currentTimeMillis()));
        this.reportHistoryRepository.save(reportHistory);
        this.reportRepository.delete(report);
        NotificationMessage scNotification = new NotificationMessage();
        scNotification.setMethod(MethodEnum.SOCKET_CLUSTER);
        SocketClusterConfiguration configuration = new SocketClusterConfiguration();
        if (request instanceof NotifyAccount) {
            configuration
                    .setChannel("domain.notify.account." + ((NotifyAccount) request).getAccount());
        } else if (request instanceof DataRequest) {
            String username = ((DataRequest) request).getHeaders().getToken().getUserData()
                    .getUsername();
            configuration.setChannel("domain.notify.user." + username);
        }
        SocketClusterData socketClusterData = new SocketClusterData();
        socketClusterData.setMethod("REPORT_FINISH");
        socketClusterData.setPayload(result);
        try {
            scNotification.setConfiguration(this.objectMapper.writeValueAsString(configuration));
            scNotification.add("socket_cluster_template", socketClusterData);
            this.requestHandler
                    .sender()
                    .sendRequestNoResponse(this.appConf.getTopic().getNotification(), "",
                            scNotification);
        } catch (Exception e) {
            log.error("fail to send notification", e);
        }
        return true;
    }

    private String getAcceptLanguage(Message message) {
        HashMap<String, HashMap<String, String>> requestHashMap = getData(message, HashMap.class);
        HashMap<String, String> headers = requestHashMap.get("headers");
        return headers.get("accept-language");
    }

    private <T> T getData(Message msg, Class<T> clazz) {
        try {
            return (T) msg.getData(objectMapper, clazz);
        } catch (IOException e) {
            log.error("fail to get data {} with clazz {}", msg, clazz, e);
            throw new GeneralException();
        }
    }
}
