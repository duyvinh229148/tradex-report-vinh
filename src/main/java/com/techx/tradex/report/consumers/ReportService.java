package com.techx.tradex.report.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.report.models.db.Report;
import com.techx.tradex.report.models.response.ReportResponse;
import com.techx.tradex.report.repositories.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

@Service
public class ReportService implements ApplicationRunner, Runnable {

    private static final Logger log = LoggerFactory.getLogger(ReportService.class);

    private ReportRepository reportRepository;
    private ObjectMapper objectMapper;
    private ReportRunnerService reportRunnerService;
    private final Object lock = new Object();

    @Autowired
    public ReportService(
            ReportRunnerService reportRunnerService,
            ReportRepository reportRepository,
            ObjectMapper objectMapper) {
        this.reportRunnerService = reportRunnerService;
        this.reportRepository = reportRepository;
        this.objectMapper = objectMapper;
    }

    public ReportResponse receiveReportRequest(Message request) {
        Report report = new Report();
        try {
            report.setJsonRequest(this.objectMapper.writeValueAsString(request));
        } catch (Exception e) {
            log.error("expect message is json serializable", e);
            throw new RuntimeException(e);
        }
        report = this.reportRepository.save(report);
        synchronized (lock) {
            lock.notify();
        }
        return new ReportResponse(report.getId());
    }

    @Override
    public void run(ApplicationArguments args) {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            boolean runned = false;
            try {
                runned = this.reportRunnerService.run();
            } catch (Exception e) {
                log.error("got error while running report", e);
            }
            if (!runned) {
                try {
                    synchronized (lock) {
                        lock.wait(5000);
                    }
                } catch (InterruptedException e) {
                    log.warn("receive InterruptedException signal. will exit thread");
                    return;
                }
            } else {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    log.warn("receive InterruptedException signal. will exit thread");
                    return;
                }
            }
        }
    }
}
