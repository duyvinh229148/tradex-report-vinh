package com.techx.tradex.report.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NumberUtil {

    public static double round2Decimal(double number) {
        return ((double) Math.round(number * 100)) / 100;
    }
}
