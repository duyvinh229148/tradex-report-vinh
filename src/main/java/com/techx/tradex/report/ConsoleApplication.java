package com.techx.tradex.report;

import com.ea.async.Async;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.report.models.request.GenerateRequest;
import com.techx.tradex.report.services.FileServerService;
import com.techx.tradex.report.services.GenerateReportService;
import java.io.File;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ConsoleApplication implements ApplicationRunner {

    @Autowired
    private GenerateReportService generateReportService;
    @Autowired
    private FileServerService fileServerService;
    @Autowired
    private ObjectMapper om;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String mode = "GEN";
        if (args.containsOption("mode")) {
            mode = args.getOptionValues("mode").get(0);
        } else {
            return;
        }

        if ("GEN".equalsIgnoreCase(mode)) {
            String templateFile = "";
            if (args.containsOption("template")) {
                templateFile = args.getOptionValues("template").get(0);
            } else {
                throw new RuntimeException("template required");
            }
            String dataFile = "";
            if (args.containsOption("json")) {
                dataFile = args.getOptionValues("json").get(0);
            } else {
                throw new RuntimeException("json required");
            }
            GenerateRequest request = new GenerateRequest();
            request.setData(this.om.readValue(new File(dataFile), Map.class));
            request.setDsType(GenerateRequest.DataSourceType.JSON);
            request.setOutputType(GenerateRequest.OutputType.PDF);
            request.setTemplate(templateFile);
            generateReportService.generateReport(request);
        } else if ("UPLOAD".equalsIgnoreCase(mode)) {
            Async.init();
            String file = args.getOptionValues("file").get(0);
            String uploadFile = new File(file).getName();
            if (args.containsOption("dstFileName")) {
                uploadFile = args.getOptionValues("dstFileName").get(0);
            }
            String s3Folder = "manual_upload/";
            if (args.containsOption("dir")) {
                s3Folder = args.getOptionValues("dir").get(0);
            }
            System.out.println(
                    fileServerService.uploadFile(new File(file), s3Folder + uploadFile, false));
        }
        System.out.println("exiting...");
        System.exit(0);
    }
}
