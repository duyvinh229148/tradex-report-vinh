package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OrderListResponse {

    private String txDate;
    private String custodycd;
    private String afacctno;
    private String stockCode;
    private String txTime;
    private String sellBuyType;
    private String status;
    private String orderType;
    private String via;
    private String tlName;
    private Double orderQuantity;
    private Double orderPrice;
    private Double execQtty;
    private Double execPrice;
    private Double unmatchedQuantity;
    private Double cancelQtty;
    private Double adjustQtty;
    private String orderNumber;
    private String feeRate;
    private String feeAmt;
    private String isAdmend;
    private String isCancel;
    private String rootOrderId;
    private String isDisposal;
    private Double remainAmt;
    private Double execAmt;
    private String subStatus;
    private String orStatusValue;
    private String refOrderId;
    private String custId;
    private String isAdvord;
}
