package com.techx.tradex.report.models.request.tradex.market;

import lombok.Data;

@Data
public class FuturesPeriodRequest {

    private String futuresCode;
    private String periodType;
    private String baseDate;
    private Integer fetchCount;
}
