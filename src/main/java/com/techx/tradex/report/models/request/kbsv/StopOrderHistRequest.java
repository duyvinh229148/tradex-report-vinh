package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class StopOrderHistRequest extends KbsvBaseRequest {

    private String accountNumber;
    private String stockCode;
    private String sellBuyType;
    private String orderType;
    private String status;
}
