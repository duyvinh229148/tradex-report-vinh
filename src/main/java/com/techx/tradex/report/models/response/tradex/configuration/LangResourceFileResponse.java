package com.techx.tradex.report.models.response.tradex.configuration;

import java.util.HashMap;
import lombok.Data;

@Data
public class LangResourceFileResponse {

    private String namespace;
    private String url;
    private HashMap<String, String> content;
}
