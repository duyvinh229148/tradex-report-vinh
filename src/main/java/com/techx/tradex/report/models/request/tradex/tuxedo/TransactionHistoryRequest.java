package com.techx.tradex.report.models.request.tradex.tuxedo;

import com.techx.tradex.common.model.requests.DataRequest;
import com.techx.tradex.report.constants.vcsc.TradingTypeEnum;
import lombok.Data;

@Data
public class TransactionHistoryRequest extends DataRequest {

    private String accountNumber;
    private String subNumber;
    private String fromDate;
    private String toDate;
    private TradingTypeEnum tradingType;
    private String lastTradingDate;
    private String lastTradingSequence;
}
