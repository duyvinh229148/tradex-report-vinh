package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OverdraftDebtHistResponse {

    private String txDate;
    private String busDate;
    private String txNum;
    private String msgAcct;
    private Double msgAmt;
    private String txStatus;
    private String txDesc;
}
