package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class FssAccountInfoResponse {

    private String fullName;
    private String idCode;
    private String idDate;
    private String idPlace;
    private String dateOfBirth;
    private String sex;
    private String address;
    private String mobile1;
    private String mobile2;
    private String email;
    private String rmName;
    private String rdName;
    private String otAuthType;
    private String otAuthTypeCode;
    private String tradeOnline;
    private String mrCrLimitMax;
    private String coreBank;
    private String bankAcctno;
    private String custodycd;
    private String custid;
    private String custRank;
    private String custAgentMobile;
    private String accountDesc;
    private String addressKBSV;
}
