package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OrderFeeAndTaxResponse {

    private String txDate;
    private String accountId;
    private String symbol;
    private String side;
    private String sideCode;
    private Double execQtty;
    private Double execPrice;
    private Double execAmt;
    private Double feeAmt;
    private Double incomeTax;
    private Double feeAcr;
    private Double incomeTaxAcr;
}
