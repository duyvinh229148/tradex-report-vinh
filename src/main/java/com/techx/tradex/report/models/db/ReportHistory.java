package com.techx.tradex.report.models.db;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "t_report_history")
@Data
public class ReportHistory {

    @Id
    private long id;

    @Column
    private String jsonRequest;

    @Column
    @Enumerated(EnumType.STRING)
    private ReportStatus status;

    @Column
    private String result;

    @Column
    private Timestamp startedAt;

    @Column
    private Timestamp createdAt;

    @Column
    private Timestamp finishedAt;
}
