package com.techx.tradex.report.models.response.kbsv;

import java.util.Map;
import lombok.Data;

@Data
public class FssAccountMapResponse {

    private Map<String, String> map;
}
