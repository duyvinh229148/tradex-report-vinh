package com.techx.tradex.report.models.request.tradex.tuxedo;

import com.techx.tradex.common.model.requests.DataRequest;
import lombok.Data;

@Data
public class AccountInfoRequest extends DataRequest {

    private String accountNumber;
    private String subNumber;
}
