package com.techx.tradex.report.models.request.tradex.configuration;

import com.techx.tradex.common.model.requests.DataRequest;
import java.util.List;
import lombok.Data;

@Data
public class LangResourceRequest extends DataRequest {

    private List<String> msNames;
}
