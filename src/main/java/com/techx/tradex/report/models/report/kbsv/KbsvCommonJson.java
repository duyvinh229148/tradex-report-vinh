package com.techx.tradex.report.models.report.kbsv;

import com.techx.tradex.report.models.report.CommonReport;
import lombok.Data;

@Data
public class KbsvCommonJson extends CommonReport {

    private String fromDate;
    private String toDate;
    private String customerName;
    private String customerId;
    private String accountId;
    private String accountDesc;
    public String filterName1;
    public String filterValue1;
    public String filterName2;
    public String filterValue2;
    public String filterName3;
    public String filterValue3;
    public String filterName4;
    public String filterValue4;
    private Object tableList;
}
