package com.techx.tradex.report.models.request.tradex.market;

import lombok.Data;

@Data
public class FuturesForeignerRequest {

    private String futuresCode;
    private String baseDate;
    private Integer fetchCount;
}
