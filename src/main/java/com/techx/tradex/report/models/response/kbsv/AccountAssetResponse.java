package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class AccountAssetResponse {

    private Double stockAmt;
    private Double balance;
    private Double cash;
    private Double buyRemainAmt;
    private Double buyAmt;
    private Double receivingT0;
    private Double receivingT1;
    private Double receivingT2;
    private Double avlAdvance;
    private Double advancedAmt;
    private Double careceiving;
    private Double CreditInterest;
    private Double totalLoan;
    private Double loanAmt;
    private Double inPayTotal;
    private Double depofeeamt;
    private Double nav;
    private Double CallRate;
    private Double ExecuteRate;
    private Double LoanLimit;
    private Double PP;
    private Double marginRate;
    private Double avlDrawDown;
    private Double buyFeeAmt;
    private Double sellFeeAmt;
    private Double sellTaxAmt;
    private Double cidepofeeacr;
}
