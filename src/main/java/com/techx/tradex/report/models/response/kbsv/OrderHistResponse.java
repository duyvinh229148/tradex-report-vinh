package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OrderHistResponse {

    private String accountId;
    private String transactionDate;
    private String clearDate;
    private String sellBuyType;
    private String symbol;
    private Double quantity;
    private Double price;
    private Double matchedAmount;
    private Double matchedPrice;
    private Double matchedValue;
    private Double transactionFeeRate;
    private Double transactionFee;
    private Double tax;
    private String status;
    private String orderingPlace;
    private String orderId;
}
