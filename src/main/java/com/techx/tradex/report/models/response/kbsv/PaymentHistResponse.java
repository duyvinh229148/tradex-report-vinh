package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class PaymentHistResponse {

    private String accountId;
    private String loanAccount;
    private String releaseDate;
    private String overdueDate;
    private String txDate;
    private Double orgLoan;
    private Double prinPaid;
    private Double interestPaid;
    private Double remain;
}
