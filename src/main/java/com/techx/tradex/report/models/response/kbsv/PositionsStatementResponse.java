package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class PositionsStatementResponse {

    private String custodycd;
    private String fullName;
    private String dmAcctNo;
    private String txDate;
    private String acctNo;
    private String tltxcd;
    private String codeId;
    private String symbol;
    private String uaCode;
    private String txDesc;
    private Double inclQtty;
    private Double otclQtty;
    private Double incsQtty;
    private Double otcsQtty;
}
