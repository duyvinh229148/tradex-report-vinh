package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class CashOnHandHistResponse {

    private String txDate;
    private String trDesc;
    private Double credit;
    private Double debit;
    private Double endCash;
}
