package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class OrderListRequest extends KbsvBaseRequest {

    private String status;
    private String sellBuyType;
}
