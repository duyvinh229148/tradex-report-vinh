package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class CollateralSdtlHistResponse {

    private String txDate;
    private String acctNo;
    private String symbol;
    private String txDesc;
    private String currDate;
    private String fullName;
    private String dmAcctNo;
    private String tltxcd;
    private String txNum;
    private Double inQtty;
    private Double otQtty;
}
