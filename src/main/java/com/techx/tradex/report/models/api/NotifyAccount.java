package com.techx.tradex.report.models.api;

public interface NotifyAccount {

    String getAccount();
}
