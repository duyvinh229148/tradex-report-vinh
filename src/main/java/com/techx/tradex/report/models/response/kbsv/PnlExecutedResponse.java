package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class PnlExecutedResponse {

    private String txDate;
    private String symbol;
    private String accountID;
    private String execType;
    private Double quantity;
    private Double execPrice;
    private Double feeAmt;
    private Double taxSellAmt;
    private Double costPrice;
    private Double pnl;
    private Double pnlRate;
    private Double costPriceValue;
    private Double execPriceValue;
}
