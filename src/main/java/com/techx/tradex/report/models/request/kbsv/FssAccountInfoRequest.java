package com.techx.tradex.report.models.request.kbsv;

import com.techx.tradex.common.model.requests.DataRequest;
import lombok.Data;

@Data
public class FssAccountInfoRequest extends DataRequest {

    private String accountId;
}
