package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class BondsToShareHistResponse {

    private String txdate;
    private String symbol;
    private Double Quantity;
    private Double amount;
    private String status;
    private Double accountId;
}
