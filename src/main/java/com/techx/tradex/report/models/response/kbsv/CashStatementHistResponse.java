package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class CashStatementHistResponse {

    private String busDate;
    private String transactionNum;
    private String transactionCode;
    private Double creditAmt;
    private Double debitAmt;
    private Double available;
    private Double beginingBalance;
    private Double endingBalance;
    private String txDesc;
}
