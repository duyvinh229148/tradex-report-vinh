package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class SecuritiesStatementRequest extends KbsvBaseRequest {

    private String code;
}
