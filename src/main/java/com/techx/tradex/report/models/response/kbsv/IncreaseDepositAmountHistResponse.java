package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class IncreaseDepositAmountHistResponse {

    public String txDate;
    public String busDate;
    public String txNum;
    public Double msgAcct;
    public String msgAmt;
    public String status;
    public String txDesc;
}
