package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OrderHistFdsResponse {

    private String custodycd;
    private String accountNo;
    private String orderNumber;
    private String stockCode;
    private String lastChange;
    private String sellBuyType;
    private String sideDesc;
    private String orderType;
    private String orderTypeDesc;
    private String validity;
    private String dataval;
    private String status;
    private Double orderQuantity;
    private Double orderPrice;
    private Double matchPrice;
    private Double matchQtty;
    private String odsent;
    private Double execAmt;
    private Double unmatchedQuantity;
    private Double cancelQtty;
    private Double admEndQtty;
    private String confirmId;
    private String originOrderId;
    private String txDate;
    private Double feeAmt;
    private Double tax;
    private Double tradingFee;
    private String via;
    private String norp;
}
