package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class PositionsCalFeeResponse {

    private String frDate;
    private String toDate;
    private String rptfrDate;
    private String rpttoDate;
    private String currDate;
    private String custodycd;
    private String fullName;
    private String acctNo;
    private String qtty;
    private String feeRate;
    private String feeAmt;
}
