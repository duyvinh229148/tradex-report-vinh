package com.techx.tradex.report.models.response.tradex.market;

import lombok.Data;

@Data
public class FuturesForeignerResponse {

    private Double last;
    private Double open;
    private Double high;
    private Double low;
    private Double change;
    private Double rate;
    private Double tradingVolume;
    private Double tradingValue;
    private String date;
    private Double foreignerBuyVolume;
    private Double foreignerSellVolume;
}
