package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class AdvancedStatementResponse {

    private String orderDate;
    private String txDate;
    private String clearDate;
    private Double amt;
    private Double advancedAmt;
    private Double feeAmt;
    private Double receiveAmt;
    private Double advancedDays;
    private String advancedStatus;
    private String advancedPlace;
}
