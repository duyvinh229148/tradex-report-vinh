package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class SecuritiesPortfolioResponse {

    private String accountID;
    private String symbol;
    private Double total;
    private Double trade;
    private Double blocked;
    private Double vsdMortgage;
    private Double receivingRight;
    private Double receivingT0;
    private Double receivingT1;
    private Double receivingT2;
    private Double costPrice;
    private Double costPriceAmt;
    private Double basicPrice;
    private Double basicPriceAmt;
    private Double pnlAmt;
    private Double pnlRate;
    private Double sendingT0;
    private Double sendingT1;
    private Double sendingT2;
}
