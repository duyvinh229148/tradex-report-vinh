package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class SellOddLotHistsResponse {

    private String accountId;
    private String symbol;
    private Double quantity;
    private Double price;
    private Double amount;
    private Double feeAmt;
    private Double taxAmt;
    private Double expReceiveAmt;
    private Double receiveAmt;
    private String status;
}
