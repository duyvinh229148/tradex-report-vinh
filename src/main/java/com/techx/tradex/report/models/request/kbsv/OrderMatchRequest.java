package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class OrderMatchRequest extends KbsvBaseRequest {

    private String code;
    private String side; //TODO
}
