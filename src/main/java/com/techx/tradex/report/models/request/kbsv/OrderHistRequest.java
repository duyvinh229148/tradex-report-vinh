package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class OrderHistRequest extends KbsvBaseRequest {

    private String code;
    private String sellBuyType;
    private String via;
    private String status;
}
