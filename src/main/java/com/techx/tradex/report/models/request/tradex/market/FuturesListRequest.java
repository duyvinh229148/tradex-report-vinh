package com.techx.tradex.report.models.request.tradex.market;

import lombok.Data;

@Data
public class FuturesListRequest {

    private String marketType;
    private String futuresCode;
    private Integer fetchCount;
}
