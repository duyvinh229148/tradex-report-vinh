package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class StopOrderHistResponse {

    private Double sequence;
    private String stockCode;
    private Double orderQuantity;
    private String sellBuyType;
    private Double stopPrice;
    private String orderType;
    private Double orderPrice;
    private String orderNumber;
    private String status;
    private String createTime;
    private String orderTime;
    private String cancelTime;
    private String errorMessage;
    private String fromDate;
    private String toDate;
}
