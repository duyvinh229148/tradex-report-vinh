package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class AccountInfoResponse {

    private String accountId;
    private Double cashOnHand;
    private Double imCash;
    private Double requiredImAmt;
    private Double requiredImAmtCcp;
    private Double requiredDmAmt;
    private Double requiredDmAmtCcp;
    private Double requiredMarginAmt;
    private Double requiredMarginAmtCcp;
    private Double eca;
    private Double ecaCcp;
    private Double prCash;
    private Double prCashCcp;
    private Double accountRatio;
    private Double accountRatioccp;
    private String accountStatus;
    private String accountStatus_Code;
    private String accountStatusCcp;
    private String accountStatusCcp_Code;
    private Double pp;
    private Double ppccp;
    private Double reqAddSecured;
    private Double reqAddSecuredCcp;
    private Double tradeFeeAmt;
    private Double tradeFeeVsd;
    private Double incometax;
    private Double taxFeeAmt;
    private Double posFeeAmt;
    private Double ecaFeeAmt;
    private Double totalFeeAmt;
    private Double overDrafAmt;
    private Double overDrafAcr;
    private Double totalOverDrafAmt;
    private Double execPnlAmt;
    private Double nonPnlAmt;
    private Double vmAmt;
    private Double vmNetAmt;
    private Double nav;
    private Double imnOnCashDta;
    private Double imnOnCashDma;
}
