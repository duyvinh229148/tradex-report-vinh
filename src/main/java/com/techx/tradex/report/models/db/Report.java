package com.techx.tradex.report.models.db;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "t_report")
@Data
public class Report {

    @Id
    @GeneratedValue
    private long id;

    @Column
    private String jsonRequest;

    @Column
    @Enumerated(EnumType.STRING)
    private ReportStatus status = ReportStatus.PENDING;

    @Column
    private String result;

    @Column
    private Timestamp startedAt;

    @Column(insertable = false)
    private Timestamp createdAt;
}
