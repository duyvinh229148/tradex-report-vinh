package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class NplByDayRequest extends KbsvBaseRequest {

    private String code;
}
