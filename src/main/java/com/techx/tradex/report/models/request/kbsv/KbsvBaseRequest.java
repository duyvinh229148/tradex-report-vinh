package com.techx.tradex.report.models.request.kbsv;

import com.techx.tradex.common.model.requests.DataRequest;
import com.techx.tradex.report.models.request.BaseGenerateRequest;
import lombok.Data;

@Data
public class KbsvBaseRequest extends DataRequest {

    private String fromDate;
    private String toDate;
    private String accountId;
    private BaseGenerateRequest.OutputType outputType;
}
