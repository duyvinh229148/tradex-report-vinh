package com.techx.tradex.report.models.request.tradex.market;

import lombok.Data;

@Data
public class IndexPeriodRequest {

    private String indexCode;
    private String periodType;
    private String baseDate;
    private Integer fetchCount;
}
