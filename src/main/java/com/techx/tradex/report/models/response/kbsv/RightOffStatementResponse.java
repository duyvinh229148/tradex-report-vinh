package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class RightOffStatementResponse {

    private String txDate;
    private String transactionNum;
    private String accountId;
    private String camastId;
    private String symbol;
    private Double qtty;
    private String status;
    private String description;
}
