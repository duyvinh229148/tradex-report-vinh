package com.techx.tradex.report.models.response.tradex.tuxedo;

@lombok.Data
public class AccountInfoResponse {

    private String customerName;
    private String identifierNumber;
    private String identifierIssueDate;
    private String identifierIssuePlace;
    private String agencyCode;
    private String email;
    private String address;
    private String phoneNumber;
    private String dateOfBirth;
    private String accountType;
    private String agencyName;
}
