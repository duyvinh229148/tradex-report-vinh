package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class LoanListResponse {

    private String accountId;
    private String loanAccount;
    private String typeName;
    private String releaseDate;
    private String overdueDate;
    private Double releaseAmt;
    private Double paid;
    private Double interestPaid;
    private Double remainDebt;
    private Double interest;
    private Double totalLoan;
    private Double days;
    private Double overdueInterest;
}
