package com.techx.tradex.report.models.response;

import com.techx.tradex.common.model.responses.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportResultResponse {

    private Long id;
    private String type;
    private Response<GenerateResponse> response;
}
