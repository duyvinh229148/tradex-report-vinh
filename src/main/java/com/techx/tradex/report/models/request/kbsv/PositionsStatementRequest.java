package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class PositionsStatementRequest extends KbsvBaseRequest {

    private String code;
    private String uaCode;
    private String tltxCd;
}
