package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class StockTransferStatementResponse {

    private String transactionDate;
    private String symbol;
    private String receiveAccount;
    private String transferAccount;
    private Double quantity;
    private String status;
}
