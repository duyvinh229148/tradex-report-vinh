package com.techx.tradex.report.models.report.vcsc;

import com.techx.tradex.report.models.report.CommonReport;
import java.util.List;
import lombok.Data;

@Data
public class TransactionStatementReportJson extends CommonReport {

    private String fromDate;
    private String toDate;
    private String customerName;
    private String accountNumberAndSub;
    private String address;
    private String sort;
    private String generatedDay;
    private String noData;

    private List<Table> tableList;

    @Data
    public static class Table {

        private String tradingDate;
        private String transactionName;
        private String stockCode;
        private Double balanceQuantity;
        private Double tradingQuantity;
        private Double tradingPrice;
        private Double tradingAmount;
        private Double fee;
        private Double loanInterest;
        private Double adjustedAmount;
        //        private String tradingSequence;
        private Double prevDepositAmount;
        private Double depositAmount;
        private String channel;
        private String remarks;
    }
}
