package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class DebtStatementResponse {

    private String frDate;
    private String toDate;
    private String currDate;
    private String fullName;
    private String vdtAcctNo;
    private String custodycd;
    private Double vOverdraftAmt;
    private String idCode;
    private String idDate;
    private String phone;
    private String idPlace;
    private String address;
    private String txDate;
    private String acctNo;
    private String txNum;
    private String txDesc;
    private String tltxcd;
    private Double ciCreditAmt;
    private Double ciDebitAmt;
    private Double ciCreditAmtAll;
    private Double ciDebitAmtAll;
    private Double ciEndAmt;
    private Double ciBalanceEnd;
    private Double ciBeginAmt;
    private Double ciCredotAmtTk;
    private Double ciDebitAmtTk;
}
