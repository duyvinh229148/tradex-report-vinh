package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class IncreaseDepositAmountHistRequest extends KbsvBaseRequest {

    private String status;
}
