package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class OrderListFdsRequest extends KbsvBaseRequest {

    private String status;
    private String sellBuyType;
}
