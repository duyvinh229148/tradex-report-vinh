package com.techx.tradex.report.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface ReportType {

    @JsonIgnore
    String getReportType();
}
