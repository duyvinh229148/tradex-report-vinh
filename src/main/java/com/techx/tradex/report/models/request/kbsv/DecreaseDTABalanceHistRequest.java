package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class DecreaseDTABalanceHistRequest extends KbsvBaseRequest {

    private String status;
}
