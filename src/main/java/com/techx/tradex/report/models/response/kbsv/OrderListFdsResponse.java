package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OrderListFdsResponse {

    private String custodycd;
    private String accountId;
    private String orderNumber;
    private String stockCode;
    private String matchTime;
    private String sellBuyType;
    private String side;
    private String orderType;
    private String orderTypeCode;
    private String validity;
    private String status;
    private String statusCode;
    private Double orderQuantity;
    private Double orderPrice;
    private Double matchPrice;
    private Double matchQtty;
    private String isCancel;
    private String isAdmend;
    private Double execAmt;
    private Double unmatchedQuantity;
    private String tlName;
    private String via;
    private String norp;
    private String confirmId;
    private Double cancelQtty;
    private Double admendQtty;
}
