package com.techx.tradex.report.models.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class FuturesReportJson {

    private List<AllChartData> wrapper;
    private String chartUri;
    private String date;
    private Vn30 vn30;
    private Futures futures;
    private List<Details> details;

    @Data
    public static class Vn30 {

        @JsonProperty("vn30_state")
        private Integer vn30State;

        @JsonProperty("vn30_change")
        private Double vn30Change;

        @JsonProperty("vn30_rate")
        private String vn30Rate;

        @JsonProperty("vn30_last")
        private Double vn30Last;

        @JsonProperty("vn30_volume")
        private Double vn30Volume;

        @JsonProperty("vn30_value")
        private Double vn30Value;
    }

    @Data
    public static class Futures {

        @JsonProperty("fLiquid")
        private Integer fLiquid;

        @JsonProperty("volume_Change")
        private Double volumeChange;

        @JsonProperty("volume_rate")
        private String volumeRate;

        @JsonProperty("volume")
        private Double volume;

        @JsonProperty("value")
        private Double value;

        @JsonProperty("value_status")
        private Integer valueStatus;

        @JsonProperty("value_Change")
        private Double valueChange;

        @JsonProperty("value_rate")
        private String valueRate;

        @JsonProperty("fgn_bought")
        private Double fgnBought;

        @JsonProperty("fgn_sold")
        private Double fgnSold;

        @JsonProperty("net_status")
        private Integer netStatus;

        @JsonProperty("net_value")
        private Double netValue;

        @JsonProperty("oi_status")
        private Integer oiStatus;

        @JsonProperty("oi_rate")
        private String oiRate;

        @JsonProperty("oi_last")
        private Double oiLast;
    }

    @Data
    public static class Details {

        @JsonProperty("code")
        private String code;

        @JsonProperty("expiry_date")
        private String expiryDate;

        @JsonProperty("oi")
        private Double OI;

        @JsonProperty("last")
        private Double last;

        @JsonProperty("change")
        private Double change;

        @JsonProperty("basis")
        private Double basis;

        @JsonProperty("t_price")
        private Double tPrice;

        @JsonProperty("volume")
        private Double volume;

        @JsonProperty("foreigners")
        private Double foreigners;
    }
}
