package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class PnlExecutedRequest extends KbsvBaseRequest {

    private String code;
}
