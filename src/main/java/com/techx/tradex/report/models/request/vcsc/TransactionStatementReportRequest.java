package com.techx.tradex.report.models.request.vcsc;

import com.techx.tradex.common.model.requests.DataRequest;
import com.techx.tradex.common.utils.validator.CombineValidator;
import com.techx.tradex.common.utils.validator.StringValidator;
import com.techx.tradex.report.constants.vcsc.TradingTypeEnum;
import com.techx.tradex.report.models.api.NotifyAccount;
import com.techx.tradex.report.models.api.ReportType;
import lombok.Data;

@Data
public class TransactionStatementReportRequest extends DataRequest
        implements NotifyAccount, ReportType {

    private String accountNumber;
    private String subNumber;
    private String fromDate;
    private String toDate;
    private TradingTypeEnum tradingType;

    public void validate() {
        new CombineValidator()
                .add(new StringValidator("subNumber", this.getSubNumber()).empty())
                .add(new StringValidator("accountNumber", this.getAccountNumber()).empty())
                .check();
    }

    @Override
    public String getAccount() {
        return accountNumber + subNumber;
    }

    @Override
    public String getReportType() {
        return "TRANS_STMT";
    }
}
