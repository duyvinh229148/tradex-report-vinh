package com.techx.tradex.report.models.response.tradex.market;

import lombok.Data;

@Data
public class FuturesPeriodResponse {

    public String date;
    public Double last;
    public Double open;
    public Double high;
    public Double low;
    public Double change;
    public Double rate;
    public Double tradingVolume;
    public Double tradingValue;
    public Double dayCount;
    public Double mBasis;
    public Double tBasis;
    public Double tPrice;
    public Double openInterest;
}
