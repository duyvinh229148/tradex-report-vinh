package com.techx.tradex.report.models.request;

import java.util.Map;
import lombok.Data;

@Data
public class GenerateRequest extends BaseGenerateRequest<Map<String, Object>> {

    protected String template;
}
