package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class NplByDayResponse {

    private String txDate;
    private String codeId;
    private String symbol;
    private String closedQtty;
    private String vwapAmt;
    private String matchAmt;
    private String vrplAmt;
    private String reliPnl;
    private String lQtty;
    private String lVwap;
    private String sQtty;
    private String sVwap;
    private String dsp;
    private Double nonClosedAmt;
    private Double nonVwapAmt;
    private String nonRplAmt;
    private String pecentNonRplAmt;
    private String dailyProfit;
}
