package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class DecreaseDepositAmountHistResponse {

    private String txDate;
    private String busDate;
    private String txNum;
    private Double msgAcct;
    private String msgAmt;
    private String status;
    private String txDesc;
}
