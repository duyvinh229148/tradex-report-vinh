package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class SecuritiesStatementResponse {

    private String txDate;
    private String busDate;
    private String symbol;
    private Double creditAmt;
    private Double debitAmt;
    private String txDesc;
    private String transactType;
}
