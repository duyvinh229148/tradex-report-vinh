package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class OrderHistFdsRequest extends KbsvBaseRequest {

    private String symbol;
    private String orderType;
    private String status;
}
