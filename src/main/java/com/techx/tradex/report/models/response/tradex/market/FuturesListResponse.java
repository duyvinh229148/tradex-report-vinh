package com.techx.tradex.report.models.response.tradex.market;

import lombok.Data;

@Data
public class FuturesListResponse {

    private String code;
    private String baseCode;
    private String baseCodeSecuritiesType;
    private String market;
    private String futuresName;
    private String futuresNameEn;
}
