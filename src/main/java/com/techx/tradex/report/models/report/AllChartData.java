package com.techx.tradex.report.models.report;

import java.util.List;
import lombok.Data;

@Data
public class AllChartData {

    private List<List<List<Double>>> allChartData;
}
