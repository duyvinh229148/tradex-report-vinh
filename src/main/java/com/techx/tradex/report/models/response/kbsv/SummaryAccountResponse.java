package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class SummaryAccountResponse {

    private Double balance;
    private Double ciBalance;
    private Double interestBalance;
    private Double caReceiving;
    private Double receivingT1;
    private Double receivingT2;
    private Double receivingT3;
    private Double securitiesAmt;
    private Double marginQttyAmt;
    private Double nonMarginQttyAmt;
    private Double totalDebtAmt;
    private Double secureAmt;
    private Double trfBuyAmt;
    private Double marginAmt;
    private Double t0DebtAmt;
    private Double advancedAmt;
    private Double depoFeeAmt;
    private Double netAssetValue;
    private Double requiredMarginAmt;
    private Double seSecuredAvl;
    private Double seSecuredBuy;
    private Double accountValue;
    private Double qttyAmt;
    private Double debtAmt;
    private Double advanceMaxAmtFee;
    private Double receivingAmt;
    private Double basicPurchasingPower;
    private Double marginRate;
    private Double holdBalance;
    private Double receivingT0;
    private Double sendingT0;
    private Double sendingT1;
    private String accountId;
    private Double avladvance;
    private Double addVnd;
    private Double buyAmt;
    private Double buyRemainValue;
    private Double avlwithdraw;
}
