package com.techx.tradex.report.models.response.tradex.tuxedo;

@lombok.Data
public class TransactionHistoryResponse {

    private String accountNumber;
    private String subNumber;
    private String tradingDate;
    private String transactionName;
    private String stockCode;
    private Double balanceQuantity;
    private Double tradingQuantity;
    private Double tradingPrice;
    private Double tradingAmount;
    private Double fee;
    private Double loanInterest;
    private Double adjustedAmount;
    private String tradingSequence;
    private Double prevDepositAmount;
    private Double depositAmount;
    private String channel;
    private String remarks;
}
