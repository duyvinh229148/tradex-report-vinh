package com.techx.tradex.report.models.request.vcsc;

import lombok.Data;

@Data
public class DomainConnectorBaseRequest {

    private String domain = "vcsc";
    private String topic = "tuxedo";
    private Object data;
}
