package com.techx.tradex.report.models.response.tradex.configuration;

import java.util.List;
import lombok.Data;

@Data
public class LangResourceResponse {

    private String msName;
    private String latestVersion;
    private String lang;
    private List<LangResourceFileResponse> files;
}

