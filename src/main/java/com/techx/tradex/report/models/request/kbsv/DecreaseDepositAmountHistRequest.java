package com.techx.tradex.report.models.request.kbsv;

import lombok.Data;

@Data
public class DecreaseDepositAmountHistRequest extends KbsvBaseRequest {

    private String status;
}
