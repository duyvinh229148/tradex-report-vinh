package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class OrderMatchResponse {

    private String txDate;
    private String acctNo;
    private String orderNumber;
    private String stockCode;
    private String sellBuyType;
    private String custodycd;
    private String matchQtty;
    private String matchPrice;
    private String matchAmt;
    private String feeAmt;
    private String taxAmt;
    private String Amt;
    private String aoFirm;
    private String norp;
}
