package com.techx.tradex.report.models.response.tradex.market;

import lombok.Data;

@Data
public class IndexPeriodResponse {

    private String code;
    private String date;
    private Double open;
    private Double high;
    private Double low;
    private Double last;
    private Double change;
    private Double rate;
    private Double tradingVolume;
    private Double tradingValue;
    private Double foreignerBuyVolume;
    private Double foreignerSellVolume;
    private Double dayCount;
}
