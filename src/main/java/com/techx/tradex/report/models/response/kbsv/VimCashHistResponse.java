package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class VimCashHistResponse {

    private String txDate;
    private String acctNo;
    private String txNum;
    private String txDesc;
    private Double ciCreditAmt;
    private Double ciDebitAmt;
    private Double availBal;
    private String custodycd;
    private Double ordNum;
}
