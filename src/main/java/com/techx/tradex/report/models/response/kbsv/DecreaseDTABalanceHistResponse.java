package com.techx.tradex.report.models.response.kbsv;

import lombok.Data;

@Data
public class DecreaseDTABalanceHistResponse {

    private String txDate;
    private String busDate;
    private String txNum;
    private Double msgAcct;
    private String msgAmt;
    private String txStatus;
    private String txDesc;
}
