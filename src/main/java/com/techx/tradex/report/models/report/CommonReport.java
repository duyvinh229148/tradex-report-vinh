package com.techx.tradex.report.models.report;

import java.util.Map;
import lombok.Data;

@Data
public class CommonReport {

    protected CommonSetting common;

    @Data
    public static class CommonSetting {

        private Map<String, String> url;
    }
}
