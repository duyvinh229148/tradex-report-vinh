package com.techx.tradex.report.models.request;

import lombok.Data;

@Data
public class BaseGenerateRequest<T> {

    public enum OutputType {
        PDF,
        XML,
        HTML,
        XLSX
    }

    public enum DataSourceType {
        JSON
    }

    protected OutputType outputType;
    protected DataSourceType dsType;
    protected T data;
    protected boolean usePhantomJs = false;
}
