package com.techx.tradex.report.models.db;

public enum ReportStatus {
    PENDING,
    RUNNING,
    SUCCESS,
    FAILED
}
