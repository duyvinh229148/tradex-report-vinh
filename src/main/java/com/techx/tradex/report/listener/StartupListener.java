package com.techx.tradex.report.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupListener implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(StartupListener.class);

    @Override
    public void run(ApplicationArguments args) {
    }
}
