package com.techx.tradex.report.services;

import com.techx.tradex.report.configurations.AppConf;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResourceService {

    private static final Logger log = LoggerFactory.getLogger(ResourceService.class);

    private AppConf appConf;
    private String phantomJsPath = null;
    private String futuresChartPath = null;

    @Autowired
    public ResourceService(AppConf appConf) {
        this.appConf = appConf;
    }

    public InputStream loadResource(String path) {
        InputStream is = getClass().getResourceAsStream(path);
        if (is == null) {
            File f = new File(path);
            if (f.exists()) {
                try {
                    return new FileInputStream(f);
                } catch (FileNotFoundException e) {
                    log.error("don't expect file not found when loading resource {}", path);
                }
            }
        }
        return null;
    }

    public String getPhantomJsPath() {
        if (phantomJsPath == null) {
            File f = new File(appConf.getPhantomjs());
            phantomJsPath = f.getAbsolutePath();
        }
        return phantomJsPath;
    }

    public String getFuturesChartPath() {
        if (futuresChartPath == null) {
            File f = new File(appConf.getFuturesChart());
            futuresChartPath = f.getAbsolutePath();
        }
        return futuresChartPath;
    }
}
