package com.techx.tradex.report.services;

import com.ea.async.Async;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.exceptions.GeneralException;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.common.model.responses.Response;
import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.constants.Constants;
import com.techx.tradex.report.consumers.RequestHandler;
import com.techx.tradex.report.models.report.AllChartData;
import com.techx.tradex.report.models.report.FuturesReportJson;
import com.techx.tradex.report.models.request.BaseGenerateRequest;
import com.techx.tradex.report.models.request.GenerateRequest;
import com.techx.tradex.report.models.request.tradex.market.FuturesForeignerRequest;
import com.techx.tradex.report.models.request.tradex.market.FuturesListRequest;
import com.techx.tradex.report.models.request.tradex.market.FuturesPeriodRequest;
import com.techx.tradex.report.models.request.tradex.market.IndexPeriodRequest;
import com.techx.tradex.report.models.response.GenerateResponse;
import com.techx.tradex.report.models.response.tradex.market.FuturesForeignerResponse;
import com.techx.tradex.report.models.response.tradex.market.FuturesListResponse;
import com.techx.tradex.report.models.response.tradex.market.FuturesPeriodResponse;
import com.techx.tradex.report.models.response.tradex.market.IndexPeriodResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

@Service
public class FuturesReportService {

    private GenerateReportService generateReportService;
    private RequestHandler requestHandler;
    private AppConf appConf;
    private ObjectMapper om;
    private ResourceService resourceService;

    @Autowired
    public FuturesReportService(
            ResourceService resourceService,
            GenerateReportService generateReportService,
            RequestHandler requestHandler,
            AppConf appConf,
            ObjectMapper om) {
        this.resourceService = resourceService;
        this.generateReportService = generateReportService;
        this.requestHandler = requestHandler;
        this.om = om;
        this.appConf = appConf;
    }

    private CompletableFuture<FuturesReportJson> queryData() {
        FuturesForeignerRequest futuresForeignerRequest = new FuturesForeignerRequest();
        IndexPeriodRequest indexPeriodRequest = new IndexPeriodRequest();
        FuturesPeriodRequest futuresPeriodRequest20 = new FuturesPeriodRequest();
        FuturesListRequest futuresListRequest = new FuturesListRequest();
        FuturesPeriodRequest futuresPeriodRequest2 = new FuturesPeriodRequest();

        FuturesReportJson jsonData = new FuturesReportJson();
        FuturesReportJson.Vn30 vn30 = new FuturesReportJson.Vn30();
        FuturesReportJson.Futures futures = new FuturesReportJson.Futures();
        FuturesReportJson.Details details0 = new FuturesReportJson.Details();
        DecimalFormat df = new DecimalFormat("0.00");

        try {
            Message futuresListMsg =
                    Async.await(
                            this.requestHandler
                                    .sender()
                                    .sendAsyncRequest(
                                            appConf.getTopic().getMarket(),
                                            appConf.getMarketRequestUri().getFuturesListUri(),
                                            null,
                                            futuresListRequest,
                                            appConf.getDefaultKafkaTimeout()));
            Response<List<FuturesListResponse>> futuresListResponse =
                    Message.getData(
                            this.om, futuresListMsg,
                            new TypeReference<Response<List<FuturesListResponse>>>() {
                            });
            List<FuturesListResponse> futuresListResponseList = futuresListResponse.getData();

            List<String> listFutures = new ArrayList<>();
            StringBuilder futuresCodeSb = new StringBuilder();
            int d = 0;
            for (FuturesListResponse response : futuresListResponseList) {
                listFutures.add(response.getCode());
                if (response.getFuturesNameEn().contains("VN30F1M")) {
                    futuresCodeSb.append(listFutures.get(d));
                }
                d++;
            }

            String futuresCode = futuresCodeSb.toString();

            futuresForeignerRequest.setFetchCount(2);
            futuresForeignerRequest.setFuturesCode(futuresCode);
            Message futuresForeignerMsg =
                    Async.await(
                            this.requestHandler
                                    .sender()
                                    .sendAsyncRequest(
                                            appConf.getTopic().getMarket(),
                                            appConf.getMarketRequestUri().getFuturesForeignerUri(),
                                            null,
                                            futuresForeignerRequest,
                                            appConf.getDefaultKafkaTimeout()));
            Response<List<FuturesForeignerResponse>> futuresForeignerResponse =
                    Message.getData(
                            this.om,
                            futuresForeignerMsg,
                            new TypeReference<Response<List<FuturesForeignerResponse>>>() {
                            });
            List<FuturesForeignerResponse> futuresForeignerResponseList =
                    futuresForeignerResponse.getData();

            indexPeriodRequest.setIndexCode("VN30");
            indexPeriodRequest.setFetchCount(1);
            indexPeriodRequest.setPeriodType("DAILY");
            Message indexPeriodMsg =
                    Async.await(
                            this.requestHandler
                                    .sender()
                                    .sendAsyncRequest(
                                            appConf.getTopic().getMarket(),
                                            appConf.getMarketRequestUri().getIndexPeriodUri(),
                                            null,
                                            indexPeriodRequest,
                                            appConf.getDefaultKafkaTimeout()));
            Response<List<IndexPeriodResponse>> indexPeriodResponse =
                    Message.getData(
                            this.om, indexPeriodMsg,
                            new TypeReference<Response<List<IndexPeriodResponse>>>() {
                            });
            List<IndexPeriodResponse> indexPeriodResponseList = indexPeriodResponse.getData();

            // daily - 20 - chart
            futuresPeriodRequest20.setFetchCount(20);
            futuresPeriodRequest20.setPeriodType("DAILY");
            futuresPeriodRequest20.setFuturesCode(futuresCode);
            Message futuresPeriodMsg =
                    Async.await(
                            this.requestHandler
                                    .sender()
                                    .sendAsyncRequest(
                                            appConf.getTopic().getMarket(),
                                            appConf.getMarketRequestUri().getFuturesPeriodUri(),
                                            null,
                                            futuresPeriodRequest20,
                                            appConf.getDefaultKafkaTimeout()));
            Response<List<FuturesPeriodResponse>> futuresPeriodResponse =
                    Message.getData(
                            this.om,
                            futuresPeriodMsg,
                            new TypeReference<Response<List<FuturesPeriodResponse>>>() {
                            });
            List<FuturesPeriodResponse> futuresPeriodResponseList = futuresPeriodResponse.getData();

            // set chart
            List<Double> oneDayData = new ArrayList<>();
            List<List<Double>> chartData = new ArrayList<>();
            for (FuturesPeriodResponse response : futuresPeriodResponseList) {
                Date date2 = new SimpleDateFormat(Constants.TRADEX_DATE_FORMAT)
                        .parse(response.getDate());
                long dateMs = date2.getTime();
                oneDayData.add((double) dateMs);
                oneDayData.add(response.getOpen());
                oneDayData.add(response.getHigh());
                oneDayData.add(response.getLow());
                oneDayData.add(response.getLast());
                oneDayData.add(response.getTradingVolume());
                chartData.add(oneDayData);
                oneDayData = new ArrayList<>();
            }
            List<List<List<Double>>> chartDataList = new ArrayList<>();
            chartDataList.add(chartData);

            AllChartData allChartData = new AllChartData();
            allChartData.setAllChartData(chartDataList);

            List<AllChartData> wrapper = new ArrayList<>();
            wrapper.add(allChartData);

            // set wrapper
            jsonData.setWrapper(wrapper);

            // set date
            String dbDate = indexPeriodResponseList.get(0).getDate();
            Date thisDate = new SimpleDateFormat(Constants.TRADEX_DATE_FORMAT).parse(dbDate);
            String thisDateString = new SimpleDateFormat(Constants.REPORT_DATE_FORMAT)
                    .format(thisDate);
            jsonData.setDate(thisDateString);

            // set vn30
            vn30.setVn30Change(indexPeriodResponseList.get(0).getChange());
            vn30.setVn30Last(indexPeriodResponseList.get(0).getLast());
            vn30.setVn30Rate(String.valueOf(indexPeriodResponseList.get(0).getRate()));
            vn30.setVn30Value(indexPeriodResponseList.get(0).getTradingValue());
            vn30.setVn30Volume(indexPeriodResponseList.get(0).getTradingVolume());
            vn30.setVn30State((indexPeriodResponseList.get(0).getChange() > 0) ? 1 : -1);

            jsonData.setVn30(vn30);

            // set futures
            List<Double> codeFgnBuy = new ArrayList<>(2);
            List<Double> codeFgnSell = new ArrayList<>(2);

            for (int i = futuresForeignerResponseList.size() - 1; i >= 0; i--) {
                codeFgnBuy.add(futuresForeignerResponseList.get(i).getForeignerBuyVolume());
                codeFgnSell.add(futuresForeignerResponseList.get(i).getForeignerSellVolume());
            }

            Double fgnNet = codeFgnBuy.get(1) - codeFgnSell.get(1);
            Integer fgnStatus = (fgnNet > 0) ? 1 : -1;

            futures.setFgnBought(codeFgnBuy.get(1));
            futures.setFgnSold(codeFgnSell.get(1));
            futures.setNetStatus(fgnStatus);
            futures.setNetValue(fgnNet);
            List<Double> codeOI = new ArrayList<>(2);
            List<Double> codeTradingVolume = new ArrayList<>(2);
            List<Double> codeTradingValue = new ArrayList<>(2);
            List<Double> codeLast = new ArrayList<>(2);

            for (int i = 0; i <= 1; i++) {
                codeOI.add(futuresPeriodResponseList.get(i).getOpenInterest());
                codeTradingVolume.add(futuresPeriodResponseList.get(i).getTradingVolume());
                codeTradingValue.add(futuresPeriodResponseList.get(i).getTradingValue());
                codeLast.add(futuresPeriodResponseList.get(i).getLast());
            }

            Double volumeChange = codeTradingVolume.get(1) - codeTradingVolume.get(0);
            Double volumeRate = 100 * volumeChange / codeTradingValue.get(0);
            Integer volStatus = (volumeChange > 0) ? 1 : -1;
            Double valueChange = codeTradingValue.get(1) - codeTradingValue.get(0);
            Integer valStatus = (valueChange > 0) ? 1 : -1;
            Double valueRate = 100 * valueChange / codeTradingValue.get(0);
            Double OIRate = 100 * (codeOI.get(1) - codeOI.get(0)) / codeOI.get(0);
            Integer OIStatus = (OIRate > 0) ? 1 : -1;

            futures.setFLiquid(volStatus);
            futures.setVolumeChange(volumeChange);
            futures.setVolumeRate(df.format(volumeRate).concat("%"));
            futures.setVolume(codeTradingVolume.get(1));
            futures.setValue(codeTradingValue.get(1));
            futures.setValueStatus(valStatus);
            futures.setValueChange(valueChange);
            futures.setValueRate((df.format(valueRate)).concat("%"));
            futures.setOiLast(codeOI.get(1));
            futures.setOiRate(df.format(OIRate).concat("%"));
            futures.setOiStatus(OIStatus);

            jsonData.setFutures(futures);

            String date =
                    futuresCode.substring(5, 7)
                            + "/"
                            + futuresCode.substring(7, 9)
                            + "/"
                            + Calendar.getInstance().get(Calendar.YEAR);

            // set details0
            Double xLast = indexPeriodResponseList.get(0).getLast();
            Double mbasis = Double.valueOf(df.format(codeLast.get(1) - xLast));
            String date1 =
                    Calendar.getInstance().get(Calendar.YEAR)
                            + futuresCode.substring(7, 9)
                            + futuresCode.substring(5, 7);
            String xDate = indexPeriodResponseList.get(0).getDate();
            int timeToMaturity = Integer.parseInt(date1) - Integer.parseInt(xDate);
            Double tPrice = Double
                    .valueOf(df.format(xLast * Math.exp(0.042 * timeToMaturity / 250)));
            Double change = codeLast.get(1) - codeLast.get(0);

            details0.setOI(codeOI.get(1));
            details0.setLast(codeLast.get(1));
            details0.setCode(futuresCode);
            details0.setExpiryDate(date);
            details0.setBasis(mbasis);
            details0.setTPrice(Double.valueOf(df.format(tPrice)));
            details0.setVolume(codeTradingVolume.get(1));
            details0.setForeigners(fgnNet);
            details0.setChange(change);

            List<FuturesReportJson.Details> detailsList = new ArrayList<>();
            detailsList.add(details0);

            jsonData.setDetails(detailsList);

            jsonData.setChartUri(resourceService.getFuturesChartPath());

        } catch (IOException | ParseException | java.text.ParseException e) {
            CompletableFuture<FuturesReportJson> future = new CompletableFuture<>();
            future.completeExceptionally(e);
            return future;
        }

        return CompletableFuture.completedFuture(jsonData);
    }

    public CompletableFuture<GenerateResponse> generateReport() {
        FuturesReportJson jsonData = Async.await(queryData());
        try {
            this.om.writeValueAsString(jsonData);
        } catch (Exception e) {
            throw new GeneralException().source(e);
        }
        // generate report
        BaseGenerateRequest<FuturesReportJson> generateRequest = new BaseGenerateRequest<>();

        InputStream is =
                getClass().getClassLoader().getResourceAsStream("reportTemplates/Futures.jrxml");
        generateRequest.setOutputType(GenerateRequest.OutputType.PDF);
        generateRequest.setDsType(GenerateRequest.DataSourceType.JSON);
        generateRequest.setData(jsonData);
        return this.generateReportService.generateReport(generateRequest, is, "Futures");
    }
}
