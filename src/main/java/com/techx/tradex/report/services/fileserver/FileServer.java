package com.techx.tradex.report.services.fileserver;

import java.io.File;

public interface FileServer {

    String uploadFile(File file, String path, boolean privateAccess);

    String uploadGzip(byte[] bytes, String path, boolean privateAccess);
}
