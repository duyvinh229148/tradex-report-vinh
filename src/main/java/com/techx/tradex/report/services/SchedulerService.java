package com.techx.tradex.report.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SchedulerService {
    private DictionaryService dictionaryService;

    @Autowired
    public SchedulerService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @Scheduled(cron = "${schedulers.resetDictionary}")
    public void resetDictionary() {
        this.dictionaryService.initDictionary();
    }
}
