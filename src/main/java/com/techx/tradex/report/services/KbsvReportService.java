package com.techx.tradex.report.services;

import com.ea.async.Async;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.common.model.requests.DataRequest;
import com.techx.tradex.common.model.responses.Response;
import com.techx.tradex.common.utils.StringUtils;
import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.constants.Constants;
import com.techx.tradex.report.consumers.RequestHandler;
import com.techx.tradex.report.models.report.CommonReport;
import com.techx.tradex.report.models.report.kbsv.KbsvCommonJson;
import com.techx.tradex.report.models.request.BaseGenerateRequest;
import com.techx.tradex.report.models.request.GenerateRequest;
import com.techx.tradex.report.models.request.kbsv.AccountAssetRequest;
import com.techx.tradex.report.models.request.kbsv.AccountInfoRequest;
import com.techx.tradex.report.models.request.kbsv.AdvancedStatementRequest;
import com.techx.tradex.report.models.request.kbsv.BondsToShareHistRequest;
import com.techx.tradex.report.models.request.kbsv.CashOnHandHistRequest;
import com.techx.tradex.report.models.request.kbsv.CashStatementHistRequest;
import com.techx.tradex.report.models.request.kbsv.CollateralSdtlHistRequest;
import com.techx.tradex.report.models.request.kbsv.DebtStatementRequest;
import com.techx.tradex.report.models.request.kbsv.DecreaseDTABalanceHistRequest;
import com.techx.tradex.report.models.request.kbsv.DecreaseDepositAmountHistRequest;
import com.techx.tradex.report.models.request.kbsv.FssAccountInfoRequest;
import com.techx.tradex.report.models.request.kbsv.FssAccountMapRequest;
import com.techx.tradex.report.models.request.kbsv.IncreaseDTABalanceHistRequest;
import com.techx.tradex.report.models.request.kbsv.IncreaseDepositAmountHistRequest;
import com.techx.tradex.report.models.request.kbsv.KbsvBaseRequest;
import com.techx.tradex.report.models.request.kbsv.LoanListRequest;
import com.techx.tradex.report.models.request.kbsv.NplByDayRequest;
import com.techx.tradex.report.models.request.kbsv.OrderFeeAndTaxRequest;
import com.techx.tradex.report.models.request.kbsv.OrderHistFdsRequest;
import com.techx.tradex.report.models.request.kbsv.OrderHistRequest;
import com.techx.tradex.report.models.request.kbsv.OrderListFdsRequest;
import com.techx.tradex.report.models.request.kbsv.OrderListRequest;
import com.techx.tradex.report.models.request.kbsv.OrderMatchRequest;
import com.techx.tradex.report.models.request.kbsv.OverdraftDebtHistRequest;
import com.techx.tradex.report.models.request.kbsv.PaymentHistRequest;
import com.techx.tradex.report.models.request.kbsv.PnlExecutedRequest;
import com.techx.tradex.report.models.request.kbsv.PositionsCalFeeRequest;
import com.techx.tradex.report.models.request.kbsv.PositionsStatementRequest;
import com.techx.tradex.report.models.request.kbsv.RightOffStatementRequest;
import com.techx.tradex.report.models.request.kbsv.SecuritiesPortfolioRequest;
import com.techx.tradex.report.models.request.kbsv.SecuritiesStatementRequest;
import com.techx.tradex.report.models.request.kbsv.SellOddLotHistsRequest;
import com.techx.tradex.report.models.request.kbsv.StockTransferStatementRequest;
import com.techx.tradex.report.models.request.kbsv.StopOrderHistRequest;
import com.techx.tradex.report.models.request.kbsv.SummaryAccountRequest;
import com.techx.tradex.report.models.request.kbsv.VimCashHistRequest;
import com.techx.tradex.report.models.response.GenerateResponse;
import com.techx.tradex.report.models.response.kbsv.AccountAssetResponse;
import com.techx.tradex.report.models.response.kbsv.AccountInfoResponse;
import com.techx.tradex.report.models.response.kbsv.AdvancedStatementResponse;
import com.techx.tradex.report.models.response.kbsv.BondsToShareHistResponse;
import com.techx.tradex.report.models.response.kbsv.CashOnHandHistResponse;
import com.techx.tradex.report.models.response.kbsv.CashStatementHistResponse;
import com.techx.tradex.report.models.response.kbsv.CollateralSdtlHistResponse;
import com.techx.tradex.report.models.response.kbsv.DebtStatementResponse;
import com.techx.tradex.report.models.response.kbsv.DecreaseDTABalanceHistResponse;
import com.techx.tradex.report.models.response.kbsv.DecreaseDepositAmountHistResponse;
import com.techx.tradex.report.models.response.kbsv.FssAccountInfoResponse;
import com.techx.tradex.report.models.response.kbsv.FssAccountMapResponse;
import com.techx.tradex.report.models.response.kbsv.IncreaseDTABalanceHistResponse;
import com.techx.tradex.report.models.response.kbsv.IncreaseDepositAmountHistResponse;
import com.techx.tradex.report.models.response.kbsv.LoanListResponse;
import com.techx.tradex.report.models.response.kbsv.NplByDayResponse;
import com.techx.tradex.report.models.response.kbsv.OrderFeeAndTaxResponse;
import com.techx.tradex.report.models.response.kbsv.OrderHistFdsResponse;
import com.techx.tradex.report.models.response.kbsv.OrderHistResponse;
import com.techx.tradex.report.models.response.kbsv.OrderListFdsResponse;
import com.techx.tradex.report.models.response.kbsv.OrderListResponse;
import com.techx.tradex.report.models.response.kbsv.OrderMatchResponse;
import com.techx.tradex.report.models.response.kbsv.OverdraftDebtHistResponse;
import com.techx.tradex.report.models.response.kbsv.PaymentHistResponse;
import com.techx.tradex.report.models.response.kbsv.PnlExecutedResponse;
import com.techx.tradex.report.models.response.kbsv.PositionsCalFeeResponse;
import com.techx.tradex.report.models.response.kbsv.PositionsStatementResponse;
import com.techx.tradex.report.models.response.kbsv.RightOffStatementResponse;
import com.techx.tradex.report.models.response.kbsv.SecuritiesPortfolioResponse;
import com.techx.tradex.report.models.response.kbsv.SecuritiesStatementResponse;
import com.techx.tradex.report.models.response.kbsv.SellOddLotHistsResponse;
import com.techx.tradex.report.models.response.kbsv.StockTransferStatementResponse;
import com.techx.tradex.report.models.response.kbsv.StopOrderHistResponse;
import com.techx.tradex.report.models.response.kbsv.SummaryAccountResponse;
import com.techx.tradex.report.models.response.kbsv.VimCashHistResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class KbsvReportService {

    private static final Logger log = LoggerFactory.getLogger(KbsvReportService.class);

    private GenerateReportService generateReportService;
    private RequestHandler requestHandler;
    private AppConf appConf;
    private ObjectMapper om;
    private DictionaryService dictionaryService;

    @Autowired
    public KbsvReportService(
            GenerateReportService generateReportService,
            RequestHandler requestHandler,
            AppConf appConf,
            ObjectMapper om,
            DictionaryService dictionaryService) {
        this.generateReportService = generateReportService;
        this.requestHandler = requestHandler;
        this.om = om;
        this.appConf = appConf;
        this.dictionaryService = dictionaryService;
    }

    private SimpleDateFormat reportDateFormat = new SimpleDateFormat(Constants.REPORT_DATE_FORMAT);
    private SimpleDateFormat reportDateTimeFormat = new SimpleDateFormat(
            Constants.REPORT_DATE_TIME_FORMAT);
    private SimpleDateFormat tradexDateFormat = new SimpleDateFormat(Constants.TRADEX_DATE_FORMAT);
    private SimpleDateFormat tradexDateTimeFormat = new SimpleDateFormat(
            Constants.TRADEX_DATE_TIME_FORMAT);

    // Bao_cao_lai_lo_da_thuc_hien
    public CompletableFuture<GenerateResponse> pnlExecuted(PnlExecutedRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, PnlExecutedResponse.class, appConf.getKbsvRequestUri().getPnlExecuted(),
                translateFieldList, acceptLanguage);
    }

    // Bao_cao_tai_san
    public CompletableFuture<GenerateResponse> accountAsset(AccountAssetRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        // this report table is not a list
        KbsvCommonJson jsonData = new KbsvCommonJson();
        setRequestDefaultValue(request);
        setJsonHeaderData(jsonData, request, acceptLanguage);

        // set table data
        AccountAssetResponse tableData =
                sendKafkaFss(
                        appConf.getKbsvRequestUri().getAccountAsset(), request,
                        AccountAssetResponse.class)
                        .join();
        jsonData.setTableList(tableData);

        // generate report
        return generateReportKbsvCommon(jsonData, request, "AccountAsset");
    }

    // Lich_su_chuyen_doi_trai_phieu
    public CompletableFuture<GenerateResponse> bondsToShareHist(BondsToShareHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, BondsToShareHistResponse.class,
                appConf.getKbsvRequestUri().getBondsToShareHist(), translateFieldList,
                acceptLanguage);
    }

    // Lich_su_chuyen_khoan_CK
    public CompletableFuture<GenerateResponse> stockTransferStatement(
            StockTransferStatementRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                StockTransferStatementResponse.class,
                appConf.getKbsvRequestUri().getStockTransferStatement(), translateFieldList,
                acceptLanguage);
    }

    // Lich_su_dang_ky_ban_CP_lo_le
    public CompletableFuture<GenerateResponse> sellOddLotHists(SellOddLotHistsRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, SellOddLotHistsResponse.class,
                appConf.getKbsvRequestUri().getSellOddLotHists(), translateFieldList,
                acceptLanguage);
    }

    // Lich_su_dang_ky_quyen_mua
    public CompletableFuture<GenerateResponse> rightOffStatement(RightOffStatementRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                RightOffStatementResponse.class,
                appConf.getKbsvRequestUri().getRightOffStatement(), translateFieldList,
                acceptLanguage);
    }

    // Lich_su_lenh
    public CompletableFuture<GenerateResponse> orderHist(OrderHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Arrays.asList("status", "sellBuyType");
        return generateReport(
                request, OrderHistResponse.class, appConf.getKbsvRequestUri().getOrderHist(),
                translateFieldList, acceptLanguage);
    }

    // Lich_su_ung_truoc_tien_ban
    public CompletableFuture<GenerateResponse> advancedStatement(AdvancedStatementRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                AdvancedStatementResponse.class,
                appConf.getKbsvRequestUri().getAdvancedStatement(), translateFieldList,
                acceptLanguage);
    }

    // Sao ke chung khoan
    public CompletableFuture<GenerateResponse> securitiesStatement(
            SecuritiesStatementRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.singletonList("transactType");
        return generateReport(
                request,
                SecuritiesStatementResponse.class,
                appConf.getKbsvRequestUri().getSecuritiesStatement(), translateFieldList,
                acceptLanguage);
    }

    // Sao ke tien
    public CompletableFuture<GenerateResponse> cashStatementHist(CashStatementHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                CashStatementHistResponse.class,
                appConf.getKbsvRequestUri().getCashStatementHist(), translateFieldList,
                acceptLanguage);
    }

    // So_du_chung_khoan
    public CompletableFuture<GenerateResponse> securitiesPortfolio(
            SecuritiesPortfolioRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        KbsvCommonJson jsonData = new KbsvCommonJson();
        setRequestDefaultValue(request);
        setJsonHeaderData(jsonData, request, acceptLanguage);

        // get account map
        Map<String, String> accountMap = getAccountMap(request);

        // get table list
        List<SecuritiesPortfolioResponse> tableList =
                sendKafkaFssList(
                        appConf.getKbsvRequestUri().getSecuritiesPortfolio(),
                        request,
                        SecuritiesPortfolioResponse.class)
                        .join();

        // add account desc to table list
        List<Object> parsedTableList = new ArrayList<>();
        tableList.forEach(
                (SecuritiesPortfolioResponse element) -> {
                    JavaType lhmType =
                            this.om
                                    .getTypeFactory()
                                    .constructParametricType(HashMap.class, String.class,
                                            Object.class);
                    HashMap<String, Object> elementMap = this.om
                            .convertValue(element, lhmType);
                    // this report use account "ID"
                    String elementAccId = (String) elementMap.get("accountID");
                    String accountDesc = accountMap.get(elementAccId);

                    elementMap.put("accountDesc", accountDesc);
                    parsedTableList.add(elementMap);
                });

        // set table data to json
        jsonData.setTableList(parsedTableList);

        // generate report
        String responseClassName = SecuritiesPortfolioResponse.class.getSimpleName();
        String reportName = responseClassName.substring(0, responseClassName.indexOf("Response"));
        return generateReportKbsvCommon(jsonData, request, reportName);
    }

    // So_du_tien
    public CompletableFuture<GenerateResponse> summaryAccount(SummaryAccountRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, SummaryAccountResponse.class,
                appConf.getKbsvRequestUri().getSummaryAccount(), translateFieldList,
                acceptLanguage);
    }

    // Thong_tin_tra_no
    public CompletableFuture<GenerateResponse> paymentHist(PaymentHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, PaymentHistResponse.class, appConf.getKbsvRequestUri().getPaymentHist(),
                translateFieldList, acceptLanguage);
    }

    // Tra cuu mon vay
    public CompletableFuture<GenerateResponse> loanList(LoanListRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, LoanListResponse.class, appConf.getKbsvRequestUri().getLoanList(),
                translateFieldList, acceptLanguage);
    }

    // Sổ lệnh thường
    public CompletableFuture<GenerateResponse> orderList(OrderListRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {

        KbsvCommonJson jsonData = new KbsvCommonJson();
        if (StringUtils.isEmpty(request.getAccountId()) || request.getAccountId().equals("ALL")) {
            request.setAccountId("ALL");
        }
        request.setFromDate(reportDateFormat.format(new Date()));
        request.setToDate(reportDateFormat.format(new Date()));
        setJsonHeaderData(jsonData, request, acceptLanguage);

        // get account map
        Map<String, String> accountMap = getAccountMap(request);

        // get table list
        List<OrderListResponse> tableList = sendKafkaFssList(
                appConf.getKbsvRequestUri().getOrderList(), request,
                OrderListResponse.class).join();

        // add account desc to table list
        List<Object> parsedTableList = new ArrayList<>();
        tableList.forEach(
                element -> {
                    JavaType type =
                            this.om
                                    .getTypeFactory()
                                    .constructParametricType(HashMap.class, String.class,
                                            Object.class);
                    HashMap<String, Object> elementMap = this.om.convertValue(element, type);
                    //accountDesc
                    String elementAccId = (String) elementMap.get("afacctno");
                    String accountDesc = accountMap.get(elementAccId);
                    elementMap.put("accountDesc", accountDesc);

                    // translate
                    List<String> fieldList = Arrays.asList("sellBuyType", "orStatusValue");
                    this.dictionaryService
                            .translateMultiField(elementMap, acceptLanguage, fieldList);
                    parsedTableList.add(elementMap);
                });

        // set table data to json
        jsonData.setTableList(parsedTableList);

        // generate report
        String responseClassName = OrderListResponse.class.getSimpleName();
        String reportName = responseClassName.substring(0, responseClassName.indexOf("Response"));
        return generateReportKbsvCommon(jsonData, request, reportName);
    }

    // Lịch sử lệnh dừng
    public CompletableFuture<GenerateResponse> stopOrderHist(StopOrderHistRequest request,
            String acceptLanguage)
            throws IOException, ParseException, IllegalAccessException, NoSuchFieldException {

        KbsvCommonJson jsonData = new KbsvCommonJson();
        if (StringUtils.isEmpty(request.getAccountId()) || request.getAccountId().equals("ALL")) {
            request.setAccountId("ALL");
        }
        setJsonHeaderData(jsonData, request, acceptLanguage);
        request.setAccountNumber(request.getAccountId());
        //this report send to topic 'order'
        request.setFromDate(
                StringUtils.isNotEmpty(request.getFromDate())
                        ? tradexDateFormat.format(reportDateFormat.parse(request.getFromDate()))
                        : tradexDateFormat.format(new Date()));
        request.setToDate(
                StringUtils.isNotEmpty(request.getToDate())
                        ? tradexDateFormat.format(reportDateFormat.parse(request.getToDate()))
                        : tradexDateFormat.format(new Date()));

        //set filter
        this.createFilter(jsonData, request, acceptLanguage);

        // get account map
        Map<String, String> accountMap = getAccountMap(request);

        // get table list
        List<StopOrderHistResponse> tableList = sendKafkaResponseList(appConf.getTopic().getOrder(),
                appConf.getKbsvRequestUri().getStopOrderHist(), request,
                StopOrderHistResponse.class).join();

        // add account desc to table list
        List<Object> parsedTableList = new ArrayList<>();
        tableList.forEach(
                element -> {
                    JavaType type =
                            this.om
                                    .getTypeFactory()
                                    .constructParametricType(HashMap.class, String.class,
                                            Object.class);
                    HashMap<String, Object> elementMap = this.om.convertValue(element, type);
                    //accountDesc
                    String elementAccId = (String) elementMap.get("accountId");
                    String accountDesc = accountMap.get(elementAccId);
                    elementMap.put("accountDesc", accountDesc);
                    //fromDate
                    String fromDate = (String) elementMap.get("fromDate");
                    String formattedFromDate = null;
                    try {
                        formattedFromDate = reportDateFormat
                                .format(tradexDateFormat.parse(fromDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    elementMap.put("fromDate", formattedFromDate);
                    //toDate
                    String toDate = (String) elementMap.get("toDate");
                    String formattedToDate = null;
                    try {
                        formattedToDate = reportDateFormat.format(tradexDateFormat.parse(toDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    elementMap.put("toDate", formattedToDate);
                    //createTime
                    String createTime = (String) elementMap.get("createTime");
                    String formattedCreateTime = null;
                    try {
                        formattedCreateTime = reportDateTimeFormat
                                .format(tradexDateTimeFormat.parse(createTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    elementMap.put("createTime", formattedCreateTime);

                    // translate
                    List<String> fieldList = Arrays.asList("orderType", "status", "sellBuyType");
                    this.dictionaryService
                            .translateMultiField(elementMap, acceptLanguage, fieldList);
                    parsedTableList.add(elementMap);
                });

        // set table data to json
        jsonData.setTableList(parsedTableList);

        // generate report
        String responseClassName = StopOrderHistResponse.class.getSimpleName();
        String reportName = responseClassName.substring(0, responseClassName.indexOf("Response"));
        return generateReportKbsvCommon(jsonData, request, reportName);
    }

    // ======================================Derivatives=========================================
    // 1. Lịch sử nộp tiền vào TKGDPS
    public CompletableFuture<GenerateResponse> increaseDTABalanceHist(
            IncreaseDTABalanceHistRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                IncreaseDTABalanceHistResponse.class,
                appConf.getKbsvRequestUri().getIncreaseDTABalanceHist(), translateFieldList,
                acceptLanguage);
    }

    // 2. Lịch sử nộp tiền vào TKKQ (CCP)
    public CompletableFuture<GenerateResponse> increaseDepositAmountHist(
            IncreaseDepositAmountHistRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                IncreaseDepositAmountHistResponse.class,
                appConf.getKbsvRequestUri().getIncreaseDepositAmountHist(), translateFieldList,
                acceptLanguage);
    }

    // 3. Lịch sử rút tiền từ TKGDPS
    public CompletableFuture<GenerateResponse> decreaseDTABalanceHist(
            DecreaseDTABalanceHistRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                DecreaseDTABalanceHistResponse.class,
                appConf.getKbsvRequestUri().getDecreaseDTABalanceHist(), translateFieldList,
                acceptLanguage);
    }

    // 4. Lịch sử rút tiền từ TKKQ (CCP)
    public CompletableFuture<GenerateResponse> decreaseDepositAmountHist(
            DecreaseDepositAmountHistRequest request, String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                DecreaseDepositAmountHistResponse.class,
                appConf.getKbsvRequestUri().getDecreaseDepositAmountHist(), translateFieldList,
                acceptLanguage);
    }

    // 5. Lịch sử tất toán nợ thấu chi
    public CompletableFuture<GenerateResponse> overdraftDebtHist(OverdraftDebtHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                OverdraftDebtHistResponse.class,
                appConf.getKbsvRequestUri().getOverdraftDebtHist(), translateFieldList,
                acceptLanguage);
    }

    // 6. Lịch sử khớp lệnh
    public CompletableFuture<GenerateResponse> orderMatch(OrderMatchRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, OrderMatchResponse.class, appConf.getKbsvRequestUri().getOrderMatch(),
                translateFieldList, acceptLanguage);
    }

    // 7. Lịch sử đặt lệnh
    public CompletableFuture<GenerateResponse> orderHistFds(OrderHistFdsRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, OrderHistFdsResponse.class, appConf.getKbsvRequestUri().getOrderHistFds(),
                translateFieldList, acceptLanguage);
    }

    // 8. Sao kê tiền TKKQ (CCP)
    public CompletableFuture<GenerateResponse> vimCashHist(VimCashHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, VimCashHistResponse.class, appConf.getKbsvRequestUri().getVimCashHist(),
                translateFieldList, acceptLanguage);
    }

    // 9. Sao kê tiền TKGDPS
    public CompletableFuture<GenerateResponse> cashOnHandHist(CashOnHandHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, CashOnHandHistResponse.class,
                appConf.getKbsvRequestUri().getCashOnHandHist(), translateFieldList,
                acceptLanguage);
    }

    // 10. Lịch sử giao dịch tài khoản đảm bảo
    public CompletableFuture<GenerateResponse> collateralSdtlHist(CollateralSdtlHistRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                CollateralSdtlHistResponse.class,
                appConf.getKbsvRequestUri().getCollateralSdtlHist(), translateFieldList,
                acceptLanguage);
    }

    // 11. Lãi lỗ hàng ngày
    public CompletableFuture<GenerateResponse> nplByDay(NplByDayRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, NplByDayResponse.class, appConf.getKbsvRequestUri().getNplByDay(),
                translateFieldList, acceptLanguage);
    }

    // 12. Sao kê giải ngân và thu nợ
    public CompletableFuture<GenerateResponse> debtStatement(DebtStatementRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, DebtStatementResponse.class,
                appConf.getKbsvRequestUri().getDebtStatement(), translateFieldList, acceptLanguage);
    }

    // 13. Sao kê vị thế
    public CompletableFuture<GenerateResponse> positionsStatement(PositionsStatementRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request,
                PositionsStatementResponse.class,
                appConf.getKbsvRequestUri().getPositionsStatement(), translateFieldList,
                acceptLanguage);
    }

    // 14. Bảng kê tính phí vị thế
    public CompletableFuture<GenerateResponse> positionsCalFee(PositionsCalFeeRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, PositionsCalFeeResponse.class,
                appConf.getKbsvRequestUri().getPositionsCalFee(), translateFieldList,
                acceptLanguage);
    }

    // 15. Bảng kê tính phí, thuế giao dịch
    public CompletableFuture<GenerateResponse> orderFeeAndTax(OrderFeeAndTaxRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        List<String> translateFieldList = Collections.emptyList();
        return generateReport(
                request, OrderFeeAndTaxResponse.class,
                appConf.getKbsvRequestUri().getOrderFeeAndTax(), translateFieldList,
                acceptLanguage);
    }

    // Sổ lệnh thường phái sinh
    public CompletableFuture<GenerateResponse> orderListFds(OrderListFdsRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {

        KbsvCommonJson jsonData = new KbsvCommonJson();
        if (StringUtils.isEmpty(request.getAccountId()) || request.getAccountId().equals("ALL")) {
            request.setAccountId("ALL");
        }
        request.setFromDate(reportDateFormat.format(new Date()));
        request.setToDate(reportDateFormat.format(new Date()));
        setJsonHeaderData(jsonData, request, acceptLanguage);

        // get account map
        Map<String, String> accountMap = getAccountMap(request);

        // get table list
        List<OrderListFdsResponse> tableList = sendKafkaFssList(
                appConf.getKbsvRequestUri().getOrderListFds(), request,
                OrderListFdsResponse.class).join();

        // add account desc to table list
        List<Object> parsedTableList = new ArrayList<>();
        tableList.forEach(
                element -> {
                    JavaType type =
                            this.om
                                    .getTypeFactory()
                                    .constructParametricType(HashMap.class, String.class,
                                            Object.class);
                    HashMap<String, Object> elementMap = this.om.convertValue(element, type);
                    //accountDesc
                    String elementAccId = (String) elementMap.get("accountId");
                    String accountDesc = accountMap.get(elementAccId);
                    elementMap.put("accountDesc", accountDesc);

                    // translate
                    List<String> fieldList = Arrays.asList("sellBuyType", "status");
                    this.dictionaryService
                            .translateMultiField(elementMap, acceptLanguage, fieldList);
                    parsedTableList.add(elementMap);
                });

        // set table data to json
        jsonData.setTableList(parsedTableList);

        // generate report
        String responseClassName = OrderListFdsResponse.class.getSimpleName();
        String reportName = responseClassName.substring(0, responseClassName.indexOf("Response"));
        return generateReportKbsvCommon(jsonData, request, reportName);
    }

    // Thông tin tài sản
    public CompletableFuture<GenerateResponse> accountInfo(AccountInfoRequest request,
            String acceptLanguage)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        // this report table is not a list
        KbsvCommonJson jsonData = new KbsvCommonJson();
        if (StringUtils.isEmpty(request.getAccountId()) || request.getAccountId().equals("ALL")) {
            request.setAccountId("ALL");
        }
        request.setFromDate(reportDateFormat.format(new Date()));
        request.setToDate(reportDateFormat.format(new Date()));
        setJsonHeaderData(jsonData, request, acceptLanguage);

        // set table data
        AccountInfoResponse tableData =
                sendKafkaFss(
                        appConf.getKbsvRequestUri().getAccountInfo(), request,
                        AccountInfoResponse.class)
                        .join();
        jsonData.setTableList(tableData);

        // generate report
        return generateReportKbsvCommon(jsonData, request, "AccountInfo");
    }

    private <T> CompletableFuture<GenerateResponse> generateReport(
            KbsvBaseRequest request,
            Class<T> responseClazz,
            String apiUrl,
            List<String> translateFieldList,
            String acceptLanguage
    )
            throws IOException, NoSuchFieldException, IllegalAccessException {
        KbsvCommonJson jsonData = new KbsvCommonJson();
        setRequestDefaultValue(request);
        setJsonHeaderData(jsonData, request, acceptLanguage);

        // get account map
        Map<String, String> accountMap = getAccountMap(request);

        // get table list
        List<T> tableList = sendKafkaFssList(apiUrl, request, responseClazz).join();

        // add account desc to table list
        List<Object> parsedTableList = new ArrayList<>();
        tableList.forEach(
                element -> {
                    JavaType type =
                            this.om
                                    .getTypeFactory()
                                    .constructParametricType(HashMap.class, String.class,
                                            Object.class);
                    HashMap<String, Object> elementMap = this.om.convertValue(element, type);
                    String elementAccId = (String) elementMap.get("accountId");
                    String accountDesc = accountMap.get(elementAccId);
                    elementMap.put("accountDesc", accountDesc);

                    //translate
                    this.dictionaryService
                            .translateMultiField(elementMap, acceptLanguage, translateFieldList);

                    parsedTableList.add(elementMap);
                });

        // set table data to json
        jsonData.setTableList(parsedTableList);

        // generate report
        String responseClassName = responseClazz.getSimpleName();
        String reportName = responseClassName.substring(0, responseClassName.indexOf("Response"));
        return generateReportKbsvCommon(jsonData, request, reportName);
    }

    private void setRequestDefaultValue(KbsvBaseRequest request) {
        if (StringUtils.isEmpty(request.getAccountId()) || request.getAccountId().equals("ALL")) {
            request.setAccountId("ALL");
        }
        request.setFromDate(
                StringUtils.isNotEmpty(request.getFromDate())
                        ? request.getFromDate()
                        : reportDateFormat.format(new Date()));
        request.setToDate(
                StringUtils.isNotEmpty(request.getToDate())
                        ? request.getToDate()
                        : reportDateFormat.format(new Date()));
    }

    private Map<String, String> getAccountMap(KbsvBaseRequest request) {
        FssAccountMapRequest accountMapRequest = new FssAccountMapRequest();
        accountMapRequest.setHeaders(request.getHeaders());

        FssAccountMapResponse accountMapResponse =
                sendKafkaFss(
                        appConf.getKbsvRequestUri().getAccountMap(),
                        accountMapRequest,
                        FssAccountMapResponse.class)
                        .join();
        if (accountMapResponse == null) {
            throw new RuntimeException("Fail to receive account map");
        }
        return accountMapResponse.getMap();
    }

    private void setJsonHeaderData(KbsvCommonJson jsonData, KbsvBaseRequest request,
            String acceptLanguage)
            throws NoSuchFieldException, IllegalAccessException {
        String accountId;
        if (StringUtils.isEmpty(request.getAccountId()) || request.getAccountId().equals("ALL")) {
            accountId = request.getHeaders().getToken().getUserData().getAccountNumbers().get(0);
        } else {
            accountId = request.getAccountId();
        }

        FssAccountInfoRequest accountInfoRequest = new FssAccountInfoRequest();

        accountInfoRequest.setAccountId(accountId);
        accountInfoRequest.setHeaders(request.getHeaders());
        FssAccountInfoResponse accountInfoResponse =
                sendKafkaFss(
                        appConf.getKbsvRequestUri().getAfacctnoInfor(),
                        accountInfoRequest,
                        FssAccountInfoResponse.class)
                        .join();

        if (accountInfoResponse == null) {
            throw new RuntimeException("Fail to receive account info: " + accountId);
        }

        // set main data
        jsonData.setCommon(new CommonReport.CommonSetting());
        jsonData.getCommon().setUrl(appConf.getReportUrls());

        //set filter
        this.createFilter(jsonData, request, acceptLanguage);

        // kbsv input date format is dd/MM/yyyy, same as reportDateFormat
        jsonData.setFromDate(request.getFromDate());
        jsonData.setToDate(request.getToDate());

        jsonData.setCustomerName(accountInfoResponse.getFullName());
        jsonData.setCustomerId(accountInfoResponse.getCustodycd());
        jsonData.setAccountId(request.getAccountId());
        if (request.getAccountId().equals("ALL")) {
            jsonData.setAccountDesc("Tất cả");
        } else {
            jsonData.setAccountDesc(accountInfoResponse.getAccountDesc());
        }
    }

    private <T> CompletableFuture<T> sendKafkaFss(
            String uri, DataRequest request, Class<T> responseClazz) {
        Message msg =
                Async.await(
                        this.requestHandler
                                .sender()
                                .sendAsyncRequest(
                                        appConf.getTopic().getFssRestBridge(),
                                        uri,
                                        null,
                                        request,
                                        appConf.getDefaultKafkaTimeout()));
        JavaType type = this.om.getTypeFactory()
                .constructParametricType(Response.class, responseClazz);
        Response<T> response = this.om.convertValue(msg.getData(), type);
        return CompletableFuture.completedFuture(response.getData());
    }

    private <T> CompletableFuture<List<T>> sendKafkaFssList(
            String uri, DataRequest request, Class<T> responseClazz) {
        Message msg =
                Async.await(
                        this.requestHandler
                                .sender()
                                .sendAsyncRequest(
                                        appConf.getTopic().getFssRestBridge(),
                                        uri,
                                        null,
                                        request,
                                        appConf.getDefaultKafkaTimeout()));
        JavaType arrayListType =
                this.om.getTypeFactory().constructParametricType(ArrayList.class, responseClazz);
        JavaType type = this.om.getTypeFactory()
                .constructParametricType(Response.class, arrayListType);

        Response<List<T>> response = this.om.convertValue(msg.getData(), type);
        if (response.getStatus() != null) {
            throw new RuntimeException(
                    "Fail to sendKafkaFssList: " + response.getStatus().getCode());
        }
        return CompletableFuture.completedFuture(response.getData());
    }

    private <T> CompletableFuture<List<T>> sendKafkaResponseList(
            String topic, String uri, DataRequest request, Class<T> responseClazz) {
        Message msg =
                Async.await(
                        this.requestHandler
                                .sender()
                                .sendAsyncRequest(
                                        topic,
                                        uri,
                                        null,
                                        request,
                                        appConf.getDefaultKafkaTimeout()));
        JavaType arrayListType =
                this.om.getTypeFactory().constructParametricType(ArrayList.class, responseClazz);
        JavaType type = this.om.getTypeFactory()
                .constructParametricType(Response.class, arrayListType);

        Response<List<T>> response = this.om.convertValue(msg.getData(), type);
        if (response.getStatus() != null) {
            throw new RuntimeException(
                    "Fail to sendKafkaFssList: " + response.getStatus().getCode());
        }
        return CompletableFuture.completedFuture(response.getData());
    }

    private <T> void createFilter(
            KbsvCommonJson jsonData,
            T request,
            String acceptLanguage
    ) throws NoSuchFieldException, IllegalAccessException {
        List<Field> filterTestedList = new ArrayList<>();

        Field[] fieldList = request.getClass().getDeclaredFields();
        for (Field field : fieldList) {
            field.setAccessible(true);
            if (appConf.getTotalFilterList().contains(field.getName())) {
                String fieldValue = (String) field.get(request);
                if (fieldValue != null) {
                    filterTestedList.add(field);
                }
            }
        }

        //set filter to json data
        int count = 1;
        for (Field field : filterTestedList) {
            String filterName = "filterName".concat(Integer.toString(count));
            Field filterNameField = KbsvCommonJson.class.getDeclaredField(filterName);
            filterNameField.setAccessible(true);
            Field translateField = appConf.getFilter().getClass().getDeclaredField(field.getName());
            translateField.setAccessible(true);
            String finalName = (String) translateField.get(appConf.getFilter());
            filterNameField.set(jsonData, finalName);

            String filterValue = "filterValue".concat(Integer.toString(count));
            Field filterValueField = KbsvCommonJson.class.getDeclaredField(filterValue);
            filterValueField.setAccessible(true);
            String requestValue = (String) field.get(request);
            if (appConf.getUnchangedFilterList().contains(field.getName())) {
                filterValueField.set(jsonData, requestValue);
            } else {
                filterValueField.set(jsonData,
                        this.dictionaryService.translate(acceptLanguage, requestValue));
            }
            count++;
        }
    }

    private CompletableFuture<GenerateResponse> generateReportKbsvCommon(
            KbsvCommonJson jsonData, KbsvBaseRequest request, String templateNameWithoutJrxml)
            throws JsonProcessingException {
        this.om.writeValueAsString(jsonData);

        BaseGenerateRequest<KbsvCommonJson> generateRequest = new BaseGenerateRequest<>();
        generateRequest.setData(jsonData);

        String resourcesUri = "reportTemplates/kbsv/";

        InputStream is =
                getClass()
                        .getClassLoader()
                        .getResourceAsStream(
                                resourcesUri.concat(templateNameWithoutJrxml).concat(".jrxml"));
        if (request.getOutputType() != null
                && request.getOutputType().equals(GenerateRequest.OutputType.XLSX)) {
            generateRequest.setOutputType(GenerateRequest.OutputType.XLSX);
        } else {
            generateRequest.setOutputType(GenerateRequest.OutputType.PDF);
        }
        generateRequest.setDsType(GenerateRequest.DataSourceType.JSON);
        return this.generateReportService
                .generateReport(generateRequest, is, templateNameWithoutJrxml);
    }
}
