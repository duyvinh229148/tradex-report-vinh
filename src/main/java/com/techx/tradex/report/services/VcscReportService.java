package com.techx.tradex.report.services;

import com.ea.async.Async;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.common.model.responses.Response;
import com.techx.tradex.common.utils.StringUtils;
import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.constants.Constants;
import com.techx.tradex.report.constants.vcsc.TradingTypeEnum;
import com.techx.tradex.report.consumers.RequestHandler;
import com.techx.tradex.report.models.report.CommonReport;
import com.techx.tradex.report.models.report.vcsc.TransactionStatementReportJson;
import com.techx.tradex.report.models.request.BaseGenerateRequest;
import com.techx.tradex.report.models.request.GenerateRequest;
import com.techx.tradex.report.models.request.tradex.tuxedo.AccountInfoRequest;
import com.techx.tradex.report.models.request.tradex.tuxedo.TransactionHistoryRequest;
import com.techx.tradex.report.models.request.vcsc.DomainConnectorBaseRequest;
import com.techx.tradex.report.models.request.vcsc.TransactionStatementReportRequest;
import com.techx.tradex.report.models.response.GenerateResponse;
import com.techx.tradex.report.models.response.tradex.tuxedo.AccountInfoResponse;
import com.techx.tradex.report.models.response.tradex.tuxedo.TransactionHistoryResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

@Service
public class VcscReportService {

    private GenerateReportService generateReportService;
    private RequestHandler requestHandler;
    private AppConf appConf;
    private ObjectMapper om;

    @Autowired
    public VcscReportService(
            GenerateReportService generateReportService,
            RequestHandler requestHandler,
            AppConf appConf,
            ObjectMapper om) {
        this.generateReportService = generateReportService;
        this.requestHandler = requestHandler;
        this.om = om;
        this.appConf = appConf;
    }

    private CompletableFuture<TransactionStatementReportJson> queryData(
            TransactionStatementReportRequest request) {
        request.validate();
        TransactionStatementReportJson jsonData = new TransactionStatementReportJson();
        SimpleDateFormat reportDateFormat = new SimpleDateFormat(Constants.REPORT_DATE_FORMAT);
        SimpleDateFormat tradexDateFormat = new SimpleDateFormat(Constants.TRADEX_DATE_FORMAT);

        TransactionHistoryRequest transactionHistoryRequest = new TransactionHistoryRequest();
        AccountInfoRequest accountInfoRequest = new AccountInfoRequest();

        try {
            // query customer info
            accountInfoRequest.setAccountNumber(request.getAccountNumber());
            accountInfoRequest.setSubNumber(request.getSubNumber());
            accountInfoRequest.setHeaders(request.getHeaders());
            Message accInfoMsg;
            if (appConf.isEnableSendDomain()) {
                DomainConnectorBaseRequest domainConnectorBaseRequest = new DomainConnectorBaseRequest();
                domainConnectorBaseRequest.setData(accountInfoRequest);
                accInfoMsg =
                        Async.await(
                                this.requestHandler
                                        .sender()
                                        .sendAsyncRequest(
                                                appConf.getTopic().getDomainConnector(),
                                                appConf.getVcscRequestUri().getAccountInfo(),
                                                null,
                                                domainConnectorBaseRequest,
                                                appConf.getDefaultKafkaTimeout()));
            } else {
                accInfoMsg =
                        Async.await(
                                this.requestHandler
                                        .sender()
                                        .sendAsyncRequest(
                                                appConf.getTopic().getTuxedo(),
                                                appConf.getVcscRequestUri().getAccountInfo(),
                                                null,
                                                accountInfoRequest,
                                                appConf.getDefaultKafkaTimeout()));
            }
            Response<AccountInfoResponse> accountInfoResponse =
                    Message.getData(
                            this.om, accInfoMsg,
                            new TypeReference<Response<AccountInfoResponse>>() {
                            });
            if (accountInfoResponse.getStatus() != null) {
                throw new RuntimeException(accountInfoResponse.getStatus().getCode());
            }
            AccountInfoResponse accountInfo = accountInfoResponse.getData();

            // set main data
            jsonData.setCommon(new CommonReport.CommonSetting());
            jsonData.getCommon().setUrl(appConf.getReportUrls());
            // vcsc input date format is yyyyMMdd, same as tradexDateFormat
            request.setFromDate(
                    StringUtils.isNotEmpty(request.getFromDate())
                            ? request.getFromDate()
                            : tradexDateFormat.format(new Date()));
            request.setToDate(
                    StringUtils.isNotEmpty(request.getToDate())
                            ? request.getToDate()
                            : tradexDateFormat.format(new Date()));
            jsonData.setFromDate(
                    reportDateFormat.format(tradexDateFormat.parse(request.getFromDate())));
            jsonData.setToDate(
                    reportDateFormat.format(tradexDateFormat.parse(request.getToDate())));

            jsonData.setCustomerName(accountInfo.getCustomerName());
            jsonData.setAccountNumberAndSub(
                    transactionHistoryRequest
                            .getAccountNumber()
                            .concat("-")
                            .concat(transactionHistoryRequest.getSubNumber()));
            jsonData.setAddress(accountInfo.getAddress());
            jsonData.setSort(
                    request.getTradingType() == null
                            ? TradingTypeEnum.ALL.getName()
                            : request.getTradingType().getName());
            jsonData.setGeneratedDay(reportDateFormat.format(new Date()));

            // query table data
            transactionHistoryRequest.setAccountNumber(request.getAccountNumber());
            transactionHistoryRequest.setSubNumber(request.getSubNumber());
            transactionHistoryRequest.setFromDate(request.getFromDate());
            transactionHistoryRequest.setToDate(request.getToDate());
            if (request.getTradingType() != null) {
                transactionHistoryRequest.setTradingType(request.getTradingType());
            }
            transactionHistoryRequest.setHeaders(request.getHeaders());

            List<TransactionHistoryResponse> responseList = new ArrayList<>();
            // send request
            List<TransactionHistoryResponse> transactionHistoryResponseList =
                    Async.await(queryTransactionHistory(transactionHistoryRequest, responseList));
            // set table data
            List<TransactionStatementReportJson.Table> tableList = new ArrayList<>();
            if (!transactionHistoryResponseList.isEmpty()) {
                for (TransactionHistoryResponse response : transactionHistoryResponseList) {
                    TransactionStatementReportJson.Table table = new TransactionStatementReportJson.Table();

                    table.setTradingDate(
                            reportDateFormat
                                    .format(tradexDateFormat.parse(response.getTradingDate())));
                    table.setTransactionName(response.getTransactionName());
                    table.setStockCode(response.getStockCode());
                    table.setBalanceQuantity(response.getBalanceQuantity());
                    table.setTradingQuantity(response.getTradingQuantity());
                    table.setTradingPrice(response.getTradingPrice());
                    table.setTradingAmount(response.getTradingAmount());
                    table.setFee(response.getFee());
                    table.setLoanInterest(response.getLoanInterest());
                    table.setAdjustedAmount(response.getAdjustedAmount());
                    table.setPrevDepositAmount(response.getPrevDepositAmount());
                    table.setDepositAmount(response.getDepositAmount());
                    table.setChannel(response.getChannel());
                    table.setRemarks(response.getRemarks());

                    tableList.add(table);
                }
            } else {
                jsonData.setNoData("true");
                TransactionStatementReportJson.Table table = new TransactionStatementReportJson.Table();
                tableList.add(table);
            }
            jsonData.setTableList(tableList);
        } catch (IOException | ParseException | java.text.ParseException e) {
            CompletableFuture<TransactionStatementReportJson> future = new CompletableFuture<>();
            future.completeExceptionally(e);
            return future;
        }
        return CompletableFuture.completedFuture(jsonData);
    }

    private CompletableFuture<List<TransactionHistoryResponse>> queryTransactionHistory(
            TransactionHistoryRequest transactionHistoryRequest,
            List<TransactionHistoryResponse> responseList)
            throws IOException {
        Message msg;
        if (appConf.isEnableSendDomain()) {
            DomainConnectorBaseRequest domainConnectorBaseRequest = new DomainConnectorBaseRequest();
            domainConnectorBaseRequest.setData(transactionHistoryRequest);
            msg =
                    Async.await(
                            this.requestHandler
                                    .sender()
                                    .sendAsyncRequest(
                                            appConf.getTopic().getDomainConnector(),
                                            appConf.getVcscRequestUri().getTransactionHistory(),
                                            null,
                                            domainConnectorBaseRequest,
                                            appConf.getDefaultKafkaTimeout()));
        } else {
            msg =
                    Async.await(
                            this.requestHandler
                                    .sender()
                                    .sendAsyncRequest(
                                            appConf.getTopic().getTuxedo(),
                                            appConf.getVcscRequestUri().getTransactionHistory(),
                                            null,
                                            transactionHistoryRequest,
                                            appConf.getDefaultKafkaTimeout()));
        }
        Response<List<TransactionHistoryResponse>> transactionHistoryResponse =
                Message.getData(
                        this.om, msg,
                        new TypeReference<Response<List<TransactionHistoryResponse>>>() {
                        });
        if (transactionHistoryResponse.getStatus() != null) {
            throw new RuntimeException(transactionHistoryResponse.getStatus().getCode());
        }
        List<TransactionHistoryResponse> transactionHistoryResponseList =
                transactionHistoryResponse.getData();
        responseList.addAll(transactionHistoryResponseList);
        // set request with lastTradingDate and lastTradingSequence
        int lastIndex = transactionHistoryResponseList.size() - 1;
        if (lastIndex == appConf.getDefaultTuxedoFetchCount() - 1) {
            transactionHistoryRequest.setLastTradingDate(
                    transactionHistoryResponseList.get(lastIndex).getTradingDate());
            transactionHistoryRequest.setLastTradingSequence(
                    transactionHistoryResponseList.get(lastIndex).getTradingSequence());
            Async.await(queryTransactionHistory(transactionHistoryRequest, responseList));
        }
        return CompletableFuture.completedFuture(responseList);
    }

    public CompletableFuture<GenerateResponse> transactionStatement(
            TransactionStatementReportRequest request) {
        TransactionStatementReportJson jsonData = Async.await(queryData(request));
        // generate report
        BaseGenerateRequest<TransactionStatementReportJson> generateRequest =
                new BaseGenerateRequest<>();
        InputStream is =
                getClass()
                        .getClassLoader()
                        .getResourceAsStream("reportTemplates/vcsc/transactionStatement.jrxml");
        generateRequest.setOutputType(GenerateRequest.OutputType.PDF);
        generateRequest.setDsType(GenerateRequest.DataSourceType.JSON);
        generateRequest.setData(jsonData);
        return this.generateReportService.generateReport(
                generateRequest, is, "vcscTransactionStatement");
    }
}
