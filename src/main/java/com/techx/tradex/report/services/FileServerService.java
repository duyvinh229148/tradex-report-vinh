package com.techx.tradex.report.services;

import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.services.fileserver.FileServer;
import com.techx.tradex.report.services.fileserver.MinioFileServer;
import com.techx.tradex.report.services.fileserver.S3FileServer;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileServerService implements FileServer {

    private static final Logger log = LoggerFactory.getLogger(FileServerService.class);

    private FileServer fileServer;

    @Autowired
    public FileServerService(
            S3FileServer s3FileServer, MinioFileServer minioFileServer, AppConf appConf) {
        if (appConf.getFileServerType() == AppConf.FileServerType.minio) {
            this.fileServer = minioFileServer;
        } else {
            this.fileServer = s3FileServer;
        }
    }

    @Override
    public String uploadFile(File file, String path, boolean privateAccess) {
        return fileServer.uploadFile(file, path, privateAccess);
    }

    @Override
    public String uploadGzip(byte[] bytes, String path, boolean privateAccess) {
        return fileServer.uploadGzip(bytes, path, privateAccess);
    }
}
