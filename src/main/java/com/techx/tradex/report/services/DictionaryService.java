package com.techx.tradex.report.services;

import com.ea.async.Async;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.common.model.requests.DataRequest;
import com.techx.tradex.common.model.responses.Response;
import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.consumers.RequestHandler;
import com.techx.tradex.report.models.request.tradex.configuration.LangResourceRequest;
import com.techx.tradex.report.models.response.tradex.configuration.LangResourceResponse;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DictionaryService {

    private static final Logger log = LoggerFactory.getLogger(DictionaryService.class);

    private AppConf appConf;
    private ObjectMapper om;
    private RequestHandler requestHandler;

    private Map<String, HashMap<String, String>> dictionary;
    private final Object lockDictionary = new Object();

    @Autowired
    public DictionaryService(AppConf appConf, ObjectMapper om, RequestHandler requestHandler) {
        this.appConf = appConf;
        this.om = om;
        this.requestHandler = requestHandler;
    }

    @PostConstruct
    public void init() {
        this.loadFromFile();
        new Thread(this::initDictionary).start();
    }

    public void initDictionary() {
        try {
            LangResourceRequest langResourceRequest = new LangResourceRequest();
            langResourceRequest.setMsNames(appConf.getNamespaceList());

            List<LangResourceResponse> responseList =
                    sendKafkaConfigurationList(
                            appConf.getTradexRequestUri().getLangResource(),
                            langResourceRequest,
                            LangResourceResponse.class)
                            .join();

            HashMap<String, HashMap<String, String>> finalHashMap = new HashMap<>();
            for (LangResourceResponse langResourceResponse : responseList) {
                String fileLang = langResourceResponse.getLang();

                HashMap<String, String> dictionaryContent = langResourceResponse.getFiles().get(0)
                        .getContent();
                HashMap<String, String> singleLanguageHashMap = finalHashMap.get(fileLang);
                if (singleLanguageHashMap == null) {
                    singleLanguageHashMap = new HashMap<>();
                }
                singleLanguageHashMap.putAll(dictionaryContent);
                finalHashMap.put(fileLang, singleLanguageHashMap);
            }
            synchronized (this.lockDictionary) {
                this.dictionary = finalHashMap;
            }
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(appConf.getDictionaryFileDir()),
                            StandardCharsets.UTF_8));
            writer.write(this.om.writeValueAsString(finalHashMap));
            writer.close();
        } catch (Exception e) {
            log.info("Fail to send kafka to configuration", e);
        }
    }

    public String translate(String language, String input) {
        final Map<String, String> matchedLanguageHM;
        synchronized (this.lockDictionary) {
            if (this.dictionary == null) {
                return input;
            }
            matchedLanguageHM = dictionary.get(language);
        }
        if (matchedLanguageHM != null) {

            HashMap<String, String> specialTranslateHM = appConf.getSpecialTranslateHM();
            String parsedInput = specialTranslateHM.get(input);
            if (parsedInput != null) {
                input = parsedInput;
            }
            if (matchedLanguageHM.get(input) != null) {
                return matchedLanguageHM.get(input);
            }
        }
        return input;
    }

    public void translateMultiField(HashMap<String, Object> elementMap, String language,
            List<String> fieldList) {
        for (String field : fieldList) {
            elementMap.put(field, this
                    .translate(language, (String) elementMap.get(field)));
        }
    }

    private void loadFromFile() {
        //read file
        File file = new File(appConf.getDictionaryFileDir());
        if (file.exists()) {
            try {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
                String dictionaryContent = br.lines().collect(Collectors.joining());

                JavaType type = this.om.getTypeFactory()
                        .constructParametricType(HashMap.class, String.class, HashMap.class);
                HashMap<String, HashMap<String, String>> finalDictionaryHM = this.om
                        .readValue(dictionaryContent, type);
                synchronized (this.lockDictionary) {
                    this.dictionary = finalDictionaryHM;
                }
            } catch (Exception e) {
                log.error("fail to load dictionary from file {}", file.getPath(), e);
            }
        }
    }

    private <T> CompletableFuture<List<T>> sendKafkaConfigurationList(
            String uri, DataRequest request, Class<T> responseClazz) {
        Message msg =
                Async.await(
                        this.requestHandler
                                .sender()
                                .sendAsyncRequest(
                                        appConf.getTopic().getConfiguration(),
                                        uri,
                                        null,
                                        request,
                                        appConf.getDefaultKafkaTimeout()));
        JavaType arrayListType =
                this.om.getTypeFactory().constructParametricType(ArrayList.class, responseClazz);
        JavaType type = this.om.getTypeFactory()
                .constructParametricType(Response.class, arrayListType);

        Response<List<T>> response = this.om.convertValue(msg.getData(), type);
        if (response.getStatus() != null) {
            throw new RuntimeException(
                    "Fail to sendKafkaConfigurationList: " + response.getStatus().getCode());
        }
        return CompletableFuture.completedFuture(response.getData());
    }
}
