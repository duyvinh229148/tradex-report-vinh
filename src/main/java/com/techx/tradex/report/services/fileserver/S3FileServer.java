package com.techx.tradex.report.services.fileserver;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.techx.tradex.report.configurations.AppConf;
import java.io.ByteArrayInputStream;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class S3FileServer implements FileServer {

    private AmazonS3 s3Client;
    private AppConf appConf;

    @Autowired
    public S3FileServer(AmazonS3 s3Client, AppConf appConf) {
        this.s3Client = s3Client;
        this.appConf = appConf;
    }

    public String uploadFile(File file, String path, boolean privateAccess) {
        this.s3Client.putObject(
                new PutObjectRequest(appConf.getS3().getBucketName(), path, file)
                        .withCannedAcl(
                                privateAccess
                                        ? CannedAccessControlList.Private
                                        : CannedAccessControlList.PublicRead));
        return s3Client.getUrl(appConf.getS3().getBucketName(), path).toString();
    }

    public String uploadGzip(byte[] bytes, String path, boolean privateAccess) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(bytes.length);
        objectMetadata.setContentEncoding("gzip");
        objectMetadata.setContentType("application/json");
        this.s3Client.putObject(
                new PutObjectRequest(
                        appConf.getS3().getBucketName(),
                        path,
                        new ByteArrayInputStream(bytes),
                        objectMetadata)
                        .withCannedAcl(
                                privateAccess
                                        ? CannedAccessControlList.Private
                                        : CannedAccessControlList.PublicRead));
        return s3Client.getUrl(appConf.getS3().getBucketName(), path).toString();
    }
}
