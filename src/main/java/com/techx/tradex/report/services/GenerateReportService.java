package com.techx.tradex.report.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.exceptions.GeneralException;
import com.techx.tradex.common.exceptions.InvalidValueException;
import com.techx.tradex.report.configurations.AppConf;
import com.techx.tradex.report.models.request.BaseGenerateRequest;
import com.techx.tradex.report.models.request.GenerateRequest;
import com.techx.tradex.report.models.response.GenerateResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import net.sf.jasperreports.customvisualization.export.CVElementPhantomJSImageDataProvider;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenerateReportService {

    private static final Logger log = LoggerFactory.getLogger(GenerateReportService.class);

    private ResourceService resourceService;
    private FileServerService fileServerService;
    private AppConf appConf;
    private ObjectMapper om;

    @Autowired
    public GenerateReportService(
            ResourceService resourceService,
            AppConf appConf,
            ObjectMapper om,
            FileServerService fileServerService) {
        this.resourceService = resourceService;
        this.appConf = appConf;
        this.fileServerService = fileServerService;
        this.om = om;
    }

    public CompletableFuture<GenerateResponse> generateReport(GenerateRequest request) {
        InputStream is = resourceService.loadResource(request.getTemplate());
        if (is == null) {
            throw new InvalidValueException("templateName");
        }
        return this.generateReport(request, is, request.getTemplate());
    }

    public CompletableFuture<GenerateResponse> generateReport(
            BaseGenerateRequest request, InputStream is, String reportName) {
        try {
            JasperReport jasperReport = JasperCompileManager.compileReport(is);

            Map<String, Object> params = new HashMap<>();
            params.put(JsonQueryExecuterFactory.JSON_DATE_PATTERN, "yyyy-MM-dd");
            params.put(JsonQueryExecuterFactory.JSON_NUMBER_PATTERN, "#,##0.##");
            params.put(JsonQueryExecuterFactory.JSON_LOCALE, Locale.ENGLISH);
            params.put(JRParameter.REPORT_LOCALE, Locale.US);

            JRDataSource ds = null;
            if (request.getDsType() == GenerateRequest.DataSourceType.JSON) {
                try {
                    String json = this.om.writeValueAsString(request.getData());
                    log.info("generate report {} with json '{}'", reportName, json);
                    ds = new JsonDataSource(
                            new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
                } catch (JsonProcessingException e) {
                    log.error("fail to parse data as json", e);
                    throw new InvalidValueException("data");
                }
            }
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

            String fileName = reportName;
            int index = fileName.lastIndexOf("/");
            if (index >= 0) {
                fileName = fileName.substring(index + 1);
            }
            fileName = fileName + "_" + System.currentTimeMillis() + "_" + Math.random();

            if (request.isUsePhantomJs()) {
                DefaultJasperReportsContext.getInstance()
                        .setProperty(
                                CVElementPhantomJSImageDataProvider.PROPERTY_PHANTOMJS_EXECUTABLE_PATH,
                                resourceService.getPhantomJsPath());
            }

            switch (request.getOutputType()) {
                case PDF:
                    fileName += ".pdf";
                    JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
                    break;
                case HTML:
                    fileName += ".html";
                    JasperExportManager.exportReportToHtmlFile(jasperPrint, fileName);
                    break;
                case XML:
                    fileName += ".xml";
                    JasperExportManager.exportReportToXmlFile(jasperPrint, fileName, true);
                    break;
                case XLSX:
                    fileName += ".xlsx";
                    SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                    configuration.setOnePagePerSheet(true);
                    configuration.setIgnoreGraphics(false);

                    try {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        OutputStream fileOutputStream = new FileOutputStream(fileName);

                        Exporter<
                                net.sf.jasperreports.export.ExporterInput,
                                net.sf.jasperreports.export.XlsxReportConfiguration,
                                net.sf.jasperreports.export.XlsxExporterConfiguration,
                                net.sf.jasperreports.export.OutputStreamExporterOutput>
                                exporter = new JRXlsxExporter();
                        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        exporter.setExporterOutput(
                                new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
                        exporter.setConfiguration(configuration);
                        exporter.exportReport();
                        byteArrayOutputStream.writeTo(fileOutputStream);
                    } catch (java.io.IOException e) {
                        throw new RuntimeException("fail to export as xlsx", e);
                    }
            }
            File f = new File(fileName);
            if (!f.exists()) {
                throw new RuntimeException("expect file exists. But not: " + fileName);
            }

            String url = fileServerService
                    .uploadFile(f, appConf.getUploadToFolder() + fileName, false);
            log.info("url to download '{}'", url);
            return CompletableFuture.completedFuture(new GenerateResponse(url));
        } catch (JRException e) {
            throw new GeneralException().source(e);
        }
    }
}
