package com.techx.tradex.report.services.fileserver;

import com.techx.tradex.report.configurations.AppConf;
import io.minio.MinioClient;
import java.io.ByteArrayInputStream;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MinioFileServer implements FileServer {

    private MinioClient minioClient;
    private AppConf appConf;

    @Autowired
    public MinioFileServer(MinioClient minioClient, AppConf appConf) {
        this.minioClient = minioClient;
        this.appConf = appConf;
    }

    public String uploadFile(File file, String path, boolean privateAccess) {
        try {
            this.minioClient.putObject(
                    appConf.getMinio().getBucketName(), file.getName(), file.getAbsolutePath());
            return appConf.getMinio().getUrlRewriteTo()
                    + appConf.getMinio().getBucketName()
                    + "/"
                    + file.getName();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String uploadGzip(byte[] bytes, String path, boolean privateAccess) {

        try {
            this.minioClient.putObject(
                    appConf.getMinio().getBucketName(),
                    path,
                    new ByteArrayInputStream(bytes),
                    "application/gzip");
            return appConf.getMinio().getUrlRewriteTo() + appConf.getMinio().getBucketName() + "/"
                    + path;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
