package com.techx.tradex.report;

import com.techx.tradex.report.configurations.AppConf;
import java.util.UUID;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfigurationProperties({AppConf.class})
@EnableScheduling
@EnableAsync
public class Application {

    public static final String instanceId = UUID.randomUUID().toString();

    public static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        applicationContext = new SpringApplication(Application.class).run(args);
    }
}
