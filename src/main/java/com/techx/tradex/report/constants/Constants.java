package com.techx.tradex.report.constants;

public interface Constants {

    String REPORT_DATE_FORMAT = "dd/MM/yyyy";
    String REPORT_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm:ss";
    String TRADEX_DATE_FORMAT = "yyyyMMdd";
    String TRADEX_DATE_TIME_FORMAT = "yyyyMMddhhmmss";
}
