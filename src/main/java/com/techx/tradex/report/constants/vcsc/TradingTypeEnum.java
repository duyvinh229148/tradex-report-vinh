package com.techx.tradex.report.constants.vcsc;

public enum TradingTypeEnum {
    ALL("Tất cả"),
    RECEIPT_PAYMENT("Tiền gửi/rút"),
    WAREHOUSING_DELIVERY("Xuất nhập kho"),
    BUY_SELL("Thanh toán bù trừ"),
    LOAN_REPAYMENT("Hoàn trả cho vay"),
    CASH_OF_RIGHT("Quyền bằng tiền mặt"),
    STOCK_OF_RIGHT("Quyền bằng cổ phiếu");

    private String name;

    TradingTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
