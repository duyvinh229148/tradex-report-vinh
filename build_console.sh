#!/bin/bash
echo "backup current application.yml"
cp src/main/resources/application.yaml src/main/resources/application.yaml.bk
echo "replace configuration"
sed -i "s|  console: false|  console: true|g" src/main/resources/application.yaml
echo "packaging"
mvn package
echo "create file"
fileName=$(ls  target/ | grep jar | grep -v "original")
mkdir -p builds
rm -f builds/console-$fileName
cp target/$fileName builds/console-$fileName
echo "return configuration file"
rm -f src/main/resources/application.yaml
mv src/main/resources/application.yaml.bk src/main/resources/application.yaml
echo "DONE"