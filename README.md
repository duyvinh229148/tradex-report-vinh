# Tradex Report
A report service for generating pdf file

## Fonts
To include font, we need to follow these steps
1. Using Jasper studio to export the fonts. You will get an jar file.
2. extract the jar file. You will get a fonts folder. An xml file under the font folder.
3. copy all directory inside the fonts directory to under directory src/main/resouces/fonts/
4. Open the xml file under the font folder. Copy all the font family tags to src/main/resouces/fonts/fontsfamily1571019013989.xml
